import torch.nn as nn
from .module import LRPModule

class ReLU(nn.ReLU, LRPModule):

    def __init__(self, inplace=False):

        nn.ReLU.__init__(self, inplace=inplace)
        LRPModule.__init__(self)

    def _lrp_simple(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_wsquare(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_gamma(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_epsilon(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

class LeakyReLU(nn.LeakyReLU, LRPModule):

    def __init__(self, inplace, negative_slope=None):
        nn.LeakyReLU.__init__(self, negative_slope=negative_slope, inplace=inplace)
        LRPModule.__init__(self)

    def _lrp_simple(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_wsquare(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_gamma(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_epsilon(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_flat(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_z(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_zb(self, Ry, y, param):
        return self.set_lrp_cache(Ry)

    def _lrp_alphabeta(self, Ry, y, param):
        return self.set_lrp_cache(Ry)