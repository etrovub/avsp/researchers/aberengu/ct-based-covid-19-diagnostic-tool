from .module import LRPModule
import torch.nn as nn

class BatchNorm2d(nn.BatchNorm2d, LRPModule):

    def __init__(self, dim_out):
        nn.BatchNorm2d.__init__(self, dim_out=dim_out)
        LRPModule.__init__(self)

    def _lrp_simple(self, Ry, y, param):
        # Batch normalization effect is handled in Conv2d LRP
        return self.set_lrp_cache(Ry)

class BatchNorm1d(nn.BatchNorm1d, LRPModule):

    def __init__(self, dim_out):
        nn.BatchNorm1d.__init__(self, num_features=dim_out)
        LRPModule.__init__(self)

    def _lrp_simple(self, Ry, y, param):
        # Batch normalization effect is handled in Linear LRP
        return self.set_lrp_cache(Ry)