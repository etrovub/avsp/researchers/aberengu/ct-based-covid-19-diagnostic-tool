import torch.nn as nn
import torch
from .module import LRPModule

class Linear(nn.Linear, LRPModule):

    def __init__(self, in_features, out_features, bias=True, following_bn_layer=None):

        nn.Linear.__init__(self, in_features=in_features, out_features=out_features, bias=bias)
        LRPModule.__init__(self)

        self.following_bn_layer = following_bn_layer
        self.epsilon = 1e-1

    def get_weights(self):
        # Return weights according to the presence of a batch_norm layer
        # See 'Breaking Batch Normalization for better explainability of Deep Neural Networks through Layer-wise Relevance Propagation'
        # for reference https://arxiv.org/pdf/2002.11018.pdf
        if self.following_bn_layer:
            sigma = self.following_bn_layer.running_var.sqrt()
            # mu = self.following_bn_layer.running_mean
            gamma = self.following_bn_layer.weight
            # beta = self.following_bn_layer.bias
            W = self.weight * gamma / (sigma + 1e-12*sigma.sign())
            return W
        else:
            return self.weight

    def get_biases(self):
        # Return biases according to the presence of a batch_norm layer

        if self.following_bn_layer:
            sigma = self.following_bn_layer.running_var.sqrt()
            mu = self.following_bn_layer.running_mean
            gamma = self.following_bn_layer.weight
            beta = self.following_bn_layer.bias
            b = beta + gamma * (self.bias - mu) / sigma
            return b
        else:
            return self.bias


    def _lrp_simple(self, Ry, y, param):

        x = self.get_input_cache()
        W = self.get_weights()
        b = self.get_biases()
        # b=None

        Z = nn.functional.linear(x, W, b)
        Z = Z + Z.sign() * 1e-12
        S = Ry / Z
        C = nn.functional.linear(S, W.t(), None)
        Rx = C * x

        return self.set_lrp_cache(Rx)

    def _lrp_wsquare(self, Ry, y, param):
        x = self.get_input_cache()
        W = torch.pow(self.get_weights(), 2)
        b = self.get_biases()

        Z = nn.functional.linear(x, W, b) + 1e-12 * torch.sign(y)
        S = Ry / Z
        C = nn.functional.linear(S, W.t(), None)
        Rx = C * x

        return self.set_lrp_cache(Rx)

    def _lrp_gamma(self, Ry, y, param):
        x = self.get_input_cache()
        W = self.get_weights()
        b = self.get_biases()
        W = W + param * torch.nn.functional.relu(W)

        Z = nn.functional.linear(x, W, b) + 1e-12 * torch.sign(y)
        S = Ry / Z
        C = nn.functional.linear(S, W.t(), None)
        Rx = C * x

        return self.set_lrp_cache(Rx)

    def _lrp_epsilon(self, Ry, y, param):

        x = self.get_input_cache()
        W = self.get_weights()
        b = self.get_biases()

        Z = nn.functional.linear(x, W, b) + param * torch.sign(y)
        S = Ry / Z
        C = nn.functional.linear(S, W.t(), None)
        Rx = C * x

        return self.set_lrp_cache(Rx)

    def _lrp_z(self, Ry, y, param):

        x = self.get_input_cache()
        W = nn.functional.relu(self.get_weights())
        b = nn.functional.relu(self.get_biases())

        Z = nn.functional.linear(x, W, b) + 1e-12 * torch.sign(y)
        S = Ry / Z
        C = nn.functional.linear(S, W.t(), None)
        Rx = C * x

        return self.set_lrp_cache(Rx)

    def _lrp_alphabeta(self, Ry, y, param):
        alpha = param[0]
        beta = param[1]

        x = self.get_input_cache()
        xp = torch.where(x >= 0.0, x, 0 * x)
        xn = torch.where(x <= 0.0, x, 0 * x)

        Wp = nn.functional.relu(self.get_weights())
        Wn = nn.functional.relu(-self.get_weights())
        bp = nn.functional.relu(self.get_biases())
        bn = nn.functional.relu(-self.get_biases())

        # Activator part
        Z1 = nn.functional.linear(xp, Wp, bp)
        Z2 = nn.functional.linear(xn, Wn, bn)
        Z = Z1 + Z2
        S = Ry / (Z + Z.sign() * 1e-12)
        C1 = nn.functional.linear(S, Wp.t(), None)
        C2 = nn.functional.linear(S, Wn.t(), None)
        Ralpha = C1 * xp + C2 * xn

        # Inhibitor part
        if beta != 0.0:
            Z1 = nn.functional.linear(xp, Wn, bn)
            Z2 = nn.functional.linear(xn, Wp, bp)
            Z = Z1 + Z2
            S = Ry / (Z + Z.sign() * 1e-12)
            C1 = nn.functional.linear(S, Wn.t(), None)
            C2 = nn.functional.linear(S, Wp.t(), None)
            Rbeta = C1 * xp + C2 * xn
        else:
            Rbeta = 0.0

        Rx = alpha * Ralpha - beta * Rbeta

        return self.set_lrp_cache(Rx)