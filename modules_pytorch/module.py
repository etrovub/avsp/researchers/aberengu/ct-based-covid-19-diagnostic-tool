import torch.nn as nn
import torch


class LRPModule:

    def __init__(self, force_lrp_type=None, lrp_param=1.0):

        self.input_cache = None
        self.lrp_cache = None
        self.lrp_param = lrp_param
        self.force_lrp_type = force_lrp_type
        self.retain_lrp = False
        self.gamma = 2
        self.epsilon = 1e-1
        self.alphabeta = (2, 1)

    def retain_lrp_cache(self):
        self.retain_lrp = True

    def get_input_cache(self):
        return self.input_cache

    def set_lrp_cache(self, R):
        if self.retain_lrp:
            self.lrp_cache = R
        torch.cuda.empty_cache() # Reduce memory leak
        return R

    def set_lrp_param(self, val):
        self.lrp_param = val

    def lrp_forward(self, x):
        self.input_cache = x
        y = self.forward(x)
        return y

    def lrp(self, Rout, hout, lrp_type='simple'):

        if self.force_lrp_type is not None:
            lrp_type = self.force_lrp_type

        if lrp_type == 'simple':
            return self._lrp_simple(Rout, hout, self.lrp_param)
        elif lrp_type == 'wsquare':
            return self._lrp_wsquare(Rout, hout, self.lrp_param)
        elif lrp_type == 'gamma':
            return self._lrp_gamma(Rout, hout, self.gamma)
        elif lrp_type == 'epsilon':
            return self._lrp_epsilon(Rout, hout, self.epsilon)
        elif lrp_type == 'flat':
            return self._lrp_flat(Rout, hout, self.lrp_param)
        elif lrp_type == 'z':
            return self._lrp_z(Rout, hout, self.lrp_param)
        elif lrp_type == 'zb':
            return self._lrp_zb(Rout, hout, self.lrp_param)
        elif lrp_type == 'alphabeta':
            return self._lrp_alphabeta(Rout, hout, self.alphabeta)
        else:
            raise Exception('Error: {} LRP rule is not existant'.format(lrp_type))

    def _lrp_simple(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_simple'.format(self.__class__.__name__))

    def _lrp_wsquare(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_wsquare'.format(self.__class__.__name__))

    def _lrp_gamma(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_gamma'.format(self.__class__.__name__))

    def _lrp_epsilon(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_epsilon'.format(self.__class__.__name__))

    def _lrp_flat(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_flat'.format(self.__class__.__name__))

    def _lrp_z(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_z'.format(self.__class__.__name__))

    def _lrp_zb(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_zb'.format(self.__class__.__name__))

    def _lrp_alphabeta(self, Ry, y, param):
        raise Exception('Error: {} does not have an implementation of _lrp_alphabeta'.format(self.__class__.__name__))
