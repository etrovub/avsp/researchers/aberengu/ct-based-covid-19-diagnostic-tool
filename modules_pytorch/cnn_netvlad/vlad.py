##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#                   Boris Joukovsky (LRP compatibility)
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB ETRO make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

from Loupe_pytorch import NetVLAD as BaseNetVLAD
from Loupe_pytorch import GatingContext as BaseGatingContext
from modules_pytorch.linear import Linear
from modules_pytorch.module import LRPModule
from modules_pytorch.utils import transfer_bn1d, transfer_sequential
import torch
import torch.functional as F


class NetVLAD(BaseNetVLAD, LRPModule):

    # LRP aware VLAD module
    # Parameters are copied from base_vlad
    # TODO: CURRENT IMPLEMENTATION DOES NOT SUPPORT BATCHNORM

    def __init__(self, base_vlad, do_gradients=None):

        assert isinstance(base_vlad, BaseNetVLAD), 'Error: {} is not of type {}'.format(base_vlad.__class__.__name__, BaseNetVLAD.__name__)
        feature_size = base_vlad.feature_size
        cluster_size = base_vlad.cluster_size
        output_dim = base_vlad.output_dim
        gating = base_vlad.gating
        add_batch_norm = base_vlad.add_batch_norm

        # assert add_batch_norm is False, 'Error: The LRP-aware NetVLAD is currently not compatible with add_batchnorm=True'

        BaseNetVLAD.__init__(self, feature_size, cluster_size, output_dim, gating, add_batch_norm)
        LRPModule.__init__(self)

        # Transferring to LRP-aware model
        if do_gradients is True:
            self.cluster_weights = torch.nn.Parameter(base_vlad.cluster_weights.data, requires_grad=True)
            self.cluster_weights2 = torch.nn.Parameter(base_vlad.cluster_weights2.data, requires_grad=True)
            self.hidden1_weights = torch.nn.Parameter(base_vlad.hidden1_weights.data, requires_grad=True)
            if base_vlad.cluster_biases is not None:
                self.cluster_biases = torch.nn.Parameter(base_vlad.cluster_biases.data, requires_grad=True)
            if base_vlad.bn1 is not None:
                self.bn1 = transfer_bn1d(base_vlad.bn1, requires_grad=do_gradients)

            if gating:
                self.context_gating = GatingContext(base_vlad.context_gating, do_gradients=do_gradients)
        else:
            self.cluster_weights = base_vlad.cluster_weights
            self.cluster_weights2 = base_vlad.cluster_weights2
            self.hidden1_weights = base_vlad.hidden1_weights
            if base_vlad.cluster_biases is not None:
                self.cluster_biases = base_vlad.cluster_biases
            if base_vlad.bn1 is not None:
                self.bn1 = transfer_bn1d(base_vlad.bn1, requires_grad=do_gradients)

            if gating:
                self.context_gating = GatingContext(base_vlad.context_gating, do_gradients=True)

    def lrp_forward(self, x, max_samples=None):

        if max_samples is None:
            assert(self.max_samples is not None), 'VLAD: max_samples is not specified'
            max_samples = self.max_samples

        self.input_cache = x

        x = x.view((-1, max_samples, self.feature_size))
        activation = torch.matmul(x, self.cluster_weights)
        '''if self.add_batch_norm:
            ###Here i must check because I'm doing the normalizattion based on the maximun number of examples
            activation = activation.view(-1, self.cluster_size)
            activation = self.bn1(activation)
            activation = activation.view(-1, max_samples, self.cluster_size)
        else:
            activation = activation + self.cluster_biases'''
        activation = activation + self.cluster_biases

        activation = self.softmax(activation)
        activation = activation.view((-1, max_samples, self.cluster_size))

        a_sum = activation.sum(-2, keepdim=True)
        a = a_sum * self.cluster_weights2

        activation = torch.transpose(activation, 2, 1)

        # Caching activation for LRP
        self.cluster_matrix_lrp = activation
        # Caching bias for LRP
        self.bias_lrp = a

        x = x.view((-1, max_samples, self.feature_size))
        vlad = torch.matmul(activation, x)
        vlad = torch.transpose(vlad, 2, 1)
        vlad = vlad - a

        # Caching unnormalized output for LRP
        self.x_hw_not_normalized = vlad

        norm = F.norm(vlad, dim=1, p=2,keepdim=True)
        vlad = vlad.div(norm.expand_as(vlad))
        ###vlad = F.norm(vlad, dim=1, p=2, keepdim=True)
        ###vlad = vlad.view((-1, self.cluster_size * self.feature_size))
        vlad = torch.reshape(vlad, [-1, self.cluster_size * self.feature_size])
        ####vlad = F.norm(vlad, dim=1, p=2,keepdim=True)
        norm = F.norm(vlad, dim=1, p=2,keepdim=True)
        vlad = vlad.div(norm.expand_as(vlad))

        # Caching activations before hidden weights
        self.x_hw = vlad

        vlad = torch.matmul(vlad, self.hidden1_weights)

        # Caching for LRP
        self.x_gc = vlad

        if self.gating:
            vlad = self.context_gating.lrp_forward(vlad)

        return vlad

    # noinspection PyUnusedLocal
    def _generic_lrp(self, Ry, y, param, lrp_type):

        # This function is not part of the default LRPModule class but I use it to facilitate the implementation
        # for different types of lrp rules

        assert(y.shape[0] == 1), 'LRP for VLAD is currently not compatible with a batch size > 1'

        #### 1) Gating context
        if self.gating:
            Ry = self.context_gating.lrp(Ry, y, lrp_type=lrp_type)

        #### 2) Hidden weights
        x = self.x_hw
        y = self.x_gc
        fake_linear_hw = Linear(in_features=x.shape[-1], out_features=y.shape[-1], bias=True, following_bn_layer=None)
        fake_linear_hw.weight = torch.nn.Parameter(self.hidden1_weights.t())
        fake_linear_hw.bias = torch.nn.Parameter(torch.zeros(y.shape[-1], dtype=torch.float32, device='cuda' if torch.cuda.is_available() else 'cpu'))
        fake_linear_hw.input_cache = x
        Ry = fake_linear_hw.lrp(Rout=Ry, hout=y, lrp_type='simple')


        #### 3) For the clustering, we will summarize the operation by a linear matrix mult with bias and normalization (bias is different for all clusters, it is a matrix)
        #### This can be further expressed as a linear layer if we consider a vectorized form of the input
        #### THIS PART IS ONLY COMPATIBLE WITH BATCH_SIZE OF 1

        # Reshape
        Ry = Ry.reshape([-1, self.feature_size, self.cluster_size])
        y = x.reshape([-1, self.feature_size, self.cluster_size])
        x = self.get_input_cache()
        n,p = x.shape[0], x.shape[1]

        # Normalization
        x_not_normalized = self.x_hw_not_normalized
        norm_fac_1 = x_not_normalized.norm(p=2, dim=1, keepdim=True)
        x_normalized1 = x_not_normalized.div(norm_fac_1.expand_as(x_not_normalized)).reshape([-1, self.cluster_size * self.feature_size])
        norm_fac_2 = x_normalized1.norm(dim=1, p=2, keepdim=True)
        normfac_glob = 1.0/(norm_fac_1.expand_as(y) * norm_fac_2.expand_as(y))
        normfac_glob = normfac_glob[0,...].detach().clone()
        assert((x_not_normalized * normfac_glob - y).norm(p=2)<1e-6)

        # Cluster matrix
        A = self.cluster_matrix_lrp.detach().clone()[0,...]

        # Bias
        b = - self.bias_lrp.detach().clone()

        # GBP*input for the feature aggregation using y = normfac+glob * (x'A' + b)

        inp = torch.nn.Parameter(self.input_cache.data.view((-1, n, p)), requires_grad=True)
        outp = (torch.transpose(torch.matmul(A, inp), 1, 2) + b) * normfac_glob
        assert((outp - y).norm(p=2)<1e-6)
        outp.backward(gradient=Ry)
        Rysum = Ry.sum()
        Ry = inp.grad * inp
        Ry = Ry/Ry.sum() * Rysum

        return self.set_lrp_cache(Ry)

        return Ry



    def _lrp_simple(self, Ry, y, param):

        return self.set_lrp_cache(self._generic_lrp(Ry, y, param, lrp_type='simple'))



class GatingContext(BaseGatingContext, LRPModule):

    def __init__(self, base_gc, do_gradients=False):

        assert isinstance(base_gc, BaseGatingContext), 'Error: {} is not of type {}'.format(
            base_gc.__class__.__name__, BaseGatingContext.__name__)
        dim = base_gc.dim

        BaseGatingContext.__init__(self, dim)
        LRPModule.__init__(self)

        # Transferring to LRP-aware model
        if do_gradients is True:
            self.gate = transfer_sequential(base_gc.gate, requires_grad=True)
        else:
            self.gate = transfer_sequential(base_gc.gate)

    def _lrp_simple(self, Ry, y, param):

        # Simple approach: Transfer all relevance to the input signal and not to the gate
        # Since we don't want to explain the gate
        # It makes sense here since a zero output to the gate should already contain zero relevance from the following layers.

        return self.set_lrp_cache(Ry)

    def lrp_forward(self, x):
        self.input_cache = x
        return self.forward(x)