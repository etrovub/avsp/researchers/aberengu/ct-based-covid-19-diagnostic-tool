##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#                   Boris Joukovsky (LRP compatibility)
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB ETRO make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

import torch.nn
from modules_pytorch.module import LRPModule
from modules import SPP as BaseSPP
import math

class SPP(BaseSPP, LRPModule):

    def __init__(self, level_pool_size, pool_operation='max'):

        BaseSPP.__init__(self, level_pool_size, pool_operation)
        LRPModule.__init__(self)

    def _lrp_simple(self, Ry, y, param):

        input = self.get_input_cache()

        if input.size()[-2] < self.level_pool_size[0] or input.size()[-1] < self.level_pool_size[0]:
            raise ValueError(
                "Pyramid pooling size can not be higher than previous filter size{}...\n".format(self.pool_operation))
        bath_size = input.size()[0]
        h, w = input.size()[-2:]

        Rx = torch.zeros_like(input).cuda()

        # 1 Find split indexes for each individual pool
        level_feats = [s[1]*s[2]*s[3] for s in self.pool_output_sizes]
        level_feats_cumul = [sum(level_feats[:i]) for i in range(len(level_feats))]
        level_feats_cumul.append(sum(level_feats))

        for i in range(len(self.level_pool_size)):

            hpool = math.ceil(h / self.level_pool_size[i])
            wpool = math.ceil(w / self.level_pool_size[i])
            hstride = math.floor(h / self.level_pool_size[i])
            wstride = math.floor(w / self.level_pool_size[i])

            # Extracting part of output
            partial_y = y[:,level_feats_cumul[i]:level_feats_cumul[i+1]].reshape([bath_size, self.pool_output_sizes[i][1], self.pool_output_sizes[i][2], self.pool_output_sizes[i][3]])
            partial_Ry = Ry[:,level_feats_cumul[i]:level_feats_cumul[i+1]].reshape([bath_size, self.pool_output_sizes[i][1], self.pool_output_sizes[i][2], self.pool_output_sizes[i][3]])

            # Reshape
            pos = self.pool_output_sizes[i]
            partial_y = partial_y.reshape((bath_size, pos[1], pos[2], pos[3]))
            partial_Ry = partial_Ry.reshape((bath_size, pos[1], pos[2], pos[3]))

            # Reverse max pool (Code borrowed from  https://github.com/sebastian-lapuschkin/lrp_toolbox/ and adapted to PyTorch)

            # assume the given pooling and stride parameters are carefully chosen.
            H = input.shape[2]
            W = input.shape[3]
            Hout = (H - hpool) // hstride + 1
            Wout = (W - wpool) // wstride + 1

            if self.pool_operation == 'max':
                for j in range(Hout):
                    for k in range(Wout):
                        Z = partial_y[:, :, j:j + 1, k:k + 1] == input[:, :, j * hstride:j * hstride + hpool,
                                                         k * wstride:k * wstride + wpool]
                        Zs = Z.sum(axis=(2, 3), keepdims=True,
                                   dtype=torch.float32)  # thanks user wodtko for reporting this bug/fix
                        Rx[:, :, j * hstride:j * hstride + hpool, k * wstride:k * wstride + wpool] += (Z / Zs) * partial_Ry[:,
                                                                                                                 :,
                                                                                                                 j:j + 1,
                                                                                                                 k:k + 1]
            else:
                raise ValueError('The specified pool_operation {} is not implemented for LRP'.format(self.pool_operation))


        return self.set_lrp_cache(Rx)
