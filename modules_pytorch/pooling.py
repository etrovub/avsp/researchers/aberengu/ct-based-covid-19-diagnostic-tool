import torch.nn as nn
import torch
import numpy as np
from .module import LRPModule


def compute_adaptive_params(in_size, out_size):
    padding, stride, kernel_size = [], [], []
    for i in range(len(in_size)):
        padding.append(0)
        stride.append(in_size[i] // out_size[i])
        kernel_size.append(in_size[i] - (out_size[i] - 1) * stride[i])

    return (padding, kernel_size, stride)


class AdaptiveAvgPool2d(nn.AdaptiveAvgPool2d, LRPModule):

    def __init__(self, output_size):
        nn.AdaptiveAvgPool2d.__init__(self, output_size=output_size)
        LRPModule.__init__(self)

    def _lrp_simple(self, Ry, y, param):

        x = self.get_input_cache()

        pad, ks, str = compute_adaptive_params(y.shape[2:], x.shape[2:])

        N, D, H, W = x.shape

        hpool, wpool = ks
        hstride, wstride = str

        # assume the given pooling and stride parameters are carefully chosen.
        Hout = int((H - hpool) / hstride + 1)
        Wout = int((W - wpool) / wstride + 1)

        Rx = torch.zeros_like(x, device='cuda' if torch.cuda.is_available() else 'cpu')

        normalizer = 1. / np.sqrt(hpool * wpool)
        R_norm = Ry / (y / normalizer + 1e-12 * (
                (y / normalizer >= 0) * 2 - 1.))  # factor in normalizer applied to Y in the forward pass

        for i in range(Hout):
            for j in range(Wout):
                Z = x[:, :, i * hstride:i * hstride + hpool, j * wstride:j * wstride + wpool]  # input activations.
                Rx[:, :, i * hstride:i * hstride + hpool:, j * wstride:j * wstride + wpool:] += Z * (
                    R_norm[:, :, i:i + 1, j:j + 1])

        return self.set_lrp_cache(Rx)

    def _lrp_gamma(self, Ry, y, param):
        return self._lrp_simple(Ry, y, param)

    def _lrp_wsquare(self, Ry, y, param):
        return self._lrp_simple(Ry, y, param)


class MaxPool2d(nn.MaxPool2d, LRPModule):

    def __init__(self, kernel_size, stride, padding):
        nn.MaxPool2d.__init__(self, kernel_size=kernel_size, stride=stride, padding=padding)
        LRPModule.__init__(self)

    def _lrp_simple(self, Ry, y, param):

        # CODE BORROWED FROM https://github.com/sebastian-lapuschkin/lrp_toolbox/

        x = self.get_input_cache()

        N, D, H, W = x.shape

        hpool, wpool = self.kernel_size
        hstride, wstride = self.stride

        # assume the given pooling and stride parameters are carefully chosen.
        Hout = (H - hpool) // hstride + 1
        Wout = (W - wpool) // wstride + 1

        Rx = torch.zeros_like(x, device= 'cuda' if torch.cuda.is_available() else 'cpu')

        for i in range(Hout):
            for j in range(Wout):
                Z = y[:, :, i:i + 1, j:j + 1] == x[:, :, i * hstride:i * hstride + hpool,
                                                 j * wstride:j * wstride + wpool]
                # Test for smoothing
                # Z[Z==0] = 0.2

                Zs = Z.sum(axis=(2, 3), keepdims=True, dtype=np.float)  # thanks user wodtko for reporting this bug/fix

                Rx[:, :, i * hstride:i * hstride + hpool, j * wstride:j * wstride + wpool] += (Z / Zs) * Ry[:, :,
                                                                                                         i:i + 1,
                                                                                                         j:j + 1]

        return self.set_lrp_cache(Rx)

    def _lrp_wsquare(self, Ry, y, param):
        '''
        Since there is no weights, simply use the simple maxpool rule
        (Original authors use the flat rule that distributes relevance evenly to the neuron receptive fields)
        '''

        return self._lrp_simple(Ry, y, param)

    def _lrp_gamma(self, Ry, y, param):
        return self._lrp_simple(Ry, y, param)

    def _lrp_flat(self, Ry, y, param):
        '''
               distribute relevance for each output evenly to the output neurons' receptive fields.
               '''

        x = self.get_input_cache()
        N, D, H, W = x.shape

        hpool, wpool = self.kernel_size
        hstride, wstride = self.stride

        # assume the given pooling and stride parameters are carefully chosen.
        Hout = (H - hpool) // hstride + 1
        Wout = (W - wpool) // wstride + 1

        Rx = torch.zeros_like(x, device='cuda' if torch.cuda.is_available() else 'cpu')

        for i in range(Hout):
            for j in range(Wout):
                # Z = torch.ones([N, D, hpool, wpool], device='cuda' if torch.cuda.is_available() else 'cpu')
                Z = x[:, :, i * hstride:i * hstride + hpool,
                                                 j * wstride:j * wstride + wpool]
                Zs = Z.sum(axis=(2,3), keepdims=True)
                Rx[:, :, i * hstride:i * hstride + hpool, j * wstride:j * wstride + wpool] += (Z / Zs) * Ry[:, :, i:i + 1, j:j + 1]
        return self.set_lrp_cache(Rx)

    def _lrp_epsilon(self, Ry, y, param):
        return self._lrp_simple(Ry, y, param)

    def _lrp_z(self, Ry, y, param):
        return self._lrp_simple(Ry, y, param)
