import torch

from . import activation, sequential, convolution, pooling, linear, normalization
import torch.nn as nn

def transfer_sequential(seq_in, requires_grad=None):
    seq_out = sequential.LRPSequential()
    i = 0

    for idx, m in enumerate(seq_in):

        # Checking for following batch_norm
        if idx < len(seq_in)-1:
            next_layer = seq_in[idx + 1]
        else:
            next_layer = None
        following_bn_layer = next_layer if isinstance(next_layer, nn.BatchNorm1d) or isinstance(next_layer, nn.BatchNorm2d) else None

        if isinstance(m, nn.Conv2d):
            n = transfer_conv2d(m, following_bn_layer=following_bn_layer, requires_grad=requires_grad)
        elif isinstance(m, nn.ReLU):
            n = activation.ReLU(inplace=m.inplace)
        elif isinstance(m, nn.LeakyReLU):
            n = transfer_leakyReLU(m, requires_grad)
        elif isinstance(m, nn.MaxPool2d):
            n = pooling.MaxPool2d(kernel_size=(m.kernel_size, m.kernel_size),
                          stride=(m.stride, m.stride))
        elif isinstance(m, nn.Linear):
            n = transfer_linear(m, following_bn_layer=following_bn_layer, requires_grad=requires_grad)
        elif isinstance(m, nn.BatchNorm2d):
            n = transfer_bn2d(m,requires_grad)
        elif isinstance(m, nn.BatchNorm1d):
            n = transfer_bn1d(m, requires_grad)
        else:
            print('Skipping layer transfer of', m.__class__)
            continue

        seq_out.add_module(str(i), n)
        i += 1

    return seq_out


# noinspection PyUnusedLocal
def transfer_conv2d(m, following_bn_layer=None, requires_grad=None):
    n = convolution.Conv2d(in_channels=m.in_channels,
                           out_channels=m.out_channels,
                           kernel_size=m.kernel_size,
                           stride=m.stride,
                           padding=m.padding,
                           following_bn_layer=following_bn_layer)
    n.weight = m.weight
    n.bias = m.bias
    return n


# noinspection PyUnusedLocal
def transfer_leakyReLU(m, requires_grad=None):
    n = activation.LeakyReLU(inplace=m.inplace, negative_slope=m.negative_slope)
    return n

def transfer_linear(m, following_bn_layer=None, requires_grad=None):
    n = linear.Linear(in_features=m.in_features, out_features=m.out_features, following_bn_layer=following_bn_layer)
    if requires_grad is True:
        n.weight = torch.nn.Parameter(m.weight.data, requires_grad=True)
        n.bias = torch.nn.Parameter(m.bias.data, requires_grad=True)
    else:
        n.weight = m.weight
        n.bias = m.bias
    return n


# noinspection PyUnusedLocal
def transfer_bn2d(m, requires_grad=None):
    n = normalization.BatchNorm2d(dim_out=m.dim_out)
    n.weight = m.weight
    n.bias = m.bias
    n.running_mean = m.running_mean
    n.running_var = m.running_var
    return n

def transfer_bn1d(m, requires_grad=None):
    n = normalization.BatchNorm1d(dim_out=m.dim_out)

    if requires_grad is True:
        n.weight = torch.nn.Parameter(m.weight.data, requires_grad=True)
        n.bias = torch.nn.Parameter(m.bias.data, requires_grad=True)
        n.running_mean = m.running_mean
        n.running_var = m.running_var
    else:
        n.weight = m.weight
        n.bias = m.bias
        n.running_mean = m.running_mean
        n.running_var = m.running_var
    return n

