import torch.nn as nn
import torch
from .module import LRPModule
import numpy as np

class Conv2d(nn.Conv2d, LRPModule):

    def __init__(self, in_channels, out_channels, kernel_size, stride=(1,1), padding=0, force_lrp=None, following_bn_layer=None):
        nn.Conv2d.__init__(self, in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding)
        LRPModule.__init__(self, force_lrp_type=force_lrp)

        self.following_bn_layer = following_bn_layer

    def get_weights(self):

        # TODO: BATCH NORM

        if self.following_bn_layer is not None:
            raise Warning('Batch norm is not yet implemented for Conv2d LRP. Continuing without batch norm.')

        return self.weight

    def get_biases(self):
        # Return biases according to the presence of a batch_norm layer

        # TODO: BATCH NORM

        return self.bias



    def _lrp_simple(self, Ry, y, param):

        # CODE INSPIRED FROM https://github.com/atulshanbhag/Layerwise-Relevance-Propagation/

        x_torch = self.get_input_cache()
        Ry_torch = Ry

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x_torch.shape[2], x_torch.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        W = self.get_weights()
        b = self.get_biases()
        Z = nn.functional.conv2d(x_torch, W, stride=self.stride, bias=b, padding=self.padding)
        Z = Z + Z.sign() * 1e-12
        S = Ry_torch / Z

        # It seems that backward pass is equivalent to a transpose convolution
        C = nn.functional.conv_transpose2d(S, W, None, self.stride, padding=self.padding, output_padding=output_padding)

        Rx = C * x_torch

        return self.set_lrp_cache(Rx)

    def _lrp_flat(self, Ry, y, param):
        x_torch = self.get_input_cache()
        # y_torch = torch.from_numpy(y).type(torch.float32)
        # Ry_torch = torch.from_numpy(Ry).type(torch.float32)
        Ry_torch = Ry

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x_torch.shape[2], x_torch.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        W = self.get_weights()
        b = self.get_biases()
        Z = nn.functional.conv2d(x_torch, torch.ones(W.shape).cuda(), stride=self.stride, bias=b, padding=self.padding)
        Z = Z + Z.sign() * 1e-12
        S = Ry_torch / Z

        # It seems that backward pass is equivalent to a transpose convolution
        C = nn.functional.conv_transpose2d(S, torch.ones(W.shape).cuda(), None, self.stride, padding=self.padding, output_padding=output_padding)

        Rx = C * x_torch

        return self.set_lrp_cache(Rx)

    def _lrp_wsquare(self, Ry, y, param):

        x_torch = self.get_input_cache()
        Ry_torch = Ry

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x_torch.shape[2], x_torch.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        W = self.get_weights()
        b = self.get_biases()
        W2 = torch.pow(W,2)
        Z = nn.functional.conv2d(x_torch, W2, stride=self.stride, bias=b, padding=self.padding) # BIAS OR NOT ?
        Z = Z + Z.sign() * 1e-12
        S = Ry_torch / Z

        # It seems that backward pass is equivalent to a transpose convolution
        C = nn.functional.conv_transpose2d(S, W2, None, self.stride, padding=self.padding, output_padding=output_padding)

        Rx = C * x_torch

        return self.set_lrp_cache(Rx)

    def _lrp_gamma(self, Ry, y, param):
        x_torch = self.get_input_cache()
        Ry_torch = Ry

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x_torch.shape[2], x_torch.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        W = self.get_weights()
        b = self.get_biases()
        W = W + param * torch.nn.functional.relu(W)

        Z = nn.functional.conv2d(x_torch, W, stride=self.stride, bias=b, padding=self.padding)
        Z = Z + Z.sign() * 1e-12
        S = Ry_torch / Z
        C = nn.functional.conv_transpose2d(S, W, None, stride=self.stride, padding=self.padding, output_padding=output_padding)
        Rx = C * x_torch

        return self.set_lrp_cache(Rx)

    def _lrp_epsilon(self, Ry, y, param):
        x_torch = self.get_input_cache()
        Ry_torch = Ry

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x_torch.shape[2], x_torch.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        W = self.get_weights()
        b = self.get_biases()
        Z = nn.functional.conv2d(x_torch, W, stride=self.stride, bias=b, padding=self.padding)
        Z = Z + Z.sign() * param
        S = Ry_torch / Z

        # It seems that backward pass is equivalent to a transpose convolution
        C = nn.functional.conv_transpose2d(S, W, None, self.stride, padding=self.padding, output_padding=output_padding)

        Rx = C * x_torch

        return self.set_lrp_cache(Rx)

    def _lrp_z(self, Ry, y, param):
        x_torch = self.get_input_cache()
        Ry_torch = Ry

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x_torch.shape[2], x_torch.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        W = nn.functional.relu(self.get_weights())
        b = nn.functional.relu(self.get_biases())
        Z = nn.functional.conv2d(x_torch, W, stride=self.stride, bias=b, padding=self.padding)
        Z = Z + Z.sign() * 1e-12
        S = Ry_torch / Z

        # It seems that backward pass is equivalent to a transpose convolution
        C = nn.functional.conv_transpose2d(S, W, None, self.stride, padding=self.padding, output_padding=output_padding)

        Rx = C * x_torch

        return self.set_lrp_cache(Rx)

    def _lrp_zb(self, Ry, y, param):

        bound_low = 0.0
        bound_high = 1.0

        x_torch = self.get_input_cache()
        Ry_torch = Ry

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x_torch.shape[2], x_torch.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        W = self.get_weights()
        Wp = nn.functional.relu(self.get_weights())
        Wn = nn.functional.relu(-self.get_weights())
        b = self.get_biases()
        bp = nn.functional.relu(self.get_biases())
        bn = nn.functional.relu(-self.get_biases())

        A = nn.functional.conv2d(x_torch, W, stride=self.stride, bias=b, padding=self.padding)
        B = nn.functional.conv2d(x_torch * 0.0 + bound_low, Wp, stride=self.stride, bias=bp, padding=self.padding)
        C = nn.functional.conv2d(x_torch * 0.0 + bound_high, Wn, stride=self.stride, bias=bn, padding=self.padding)

        Z = A - B - C
        Z = Z + Z.sign() * 1e-12
        S = Ry_torch / Z

        # It seems that backward pass is equivalent to a transpose convolution
        CA = nn.functional.conv_transpose2d(S, W, None, self.stride, padding=self.padding, output_padding=output_padding)
        CB = nn.functional.conv_transpose2d(S, Wp, None, self.stride, padding=self.padding, output_padding=output_padding)
        CC = nn.functional.conv_transpose2d(S, Wn, None, self.stride, padding=self.padding, output_padding=output_padding)

        Rx = CA * x_torch - CB * (x_torch * 0.0 + bound_low) - CC * (x_torch * 0.0 + bound_high)

        return self.set_lrp_cache(Rx)

    def _lrp_alphabeta(self, Ry, y, param):

        alpha = param[0]
        beta = param[1]

        x = self.get_input_cache()
        xp = torch.where(x>=0.0, x, 0*x)
        xn = torch.where(x<=0.0, x, 0*x)

        output_padding = nn.ConvTranspose2d._output_padding(
            self=None,
            input=y,
            output_size=[x.shape[2], x.shape[3]],
            stride=self.stride,
            padding=self.padding,
            kernel_size=self.kernel_size
        )

        Wp = nn.functional.relu(self.get_weights())
        Wn = nn.functional.relu(-self.get_weights())
        bp = nn.functional.relu(self.get_biases())
        bn = nn.functional.relu(-self.get_biases())
        # bp = None
        # bn = None

        # Activator part
        Z1 = nn.functional.conv2d(xp, Wp, stride=self.stride, bias=bp, padding=self.padding)
        Z2 = nn.functional.conv2d(xn, Wn, stride=self.stride, bias=bn, padding=self.padding)
        Z = Z1 + Z2
        S = Ry / (Z + Z.sign()*1e-12)
        C1 = nn.functional.conv_transpose2d(S, Wp, stride=self.stride, bias=None, padding=self.padding, output_padding=output_padding)
        C2 = nn.functional.conv_transpose2d(S, Wn, stride=self.stride, bias=None, padding=self.padding,
                                            output_padding=output_padding)
        Ralpha = C1 * xp + C2 * xn

        # Inhibitor part
        if beta != 0.0:
            Z1 = nn.functional.conv2d(xp, Wn, stride=self.stride, bias=bn, padding=self.padding)
            Z2 = nn.functional.conv2d(xn, Wp, stride=self.stride, bias=bp, padding=self.padding)
            Z = Z1 + Z2
            S = Ry / (Z + Z.sign() * 1e-12)
            C1 = nn.functional.conv_transpose2d(S, Wn, stride=self.stride, bias=None, padding=self.padding,
                                                output_padding=output_padding)
            C2 = nn.functional.conv_transpose2d(S, Wp, stride=self.stride, bias=None, padding=self.padding,
                                                output_padding=output_padding)
            Rbeta = C1 * xp + C2 * xn
        else:
            Rbeta = 0.0

        Rx = alpha * Ralpha - beta * Rbeta

        return self.set_lrp_cache(Rx)
