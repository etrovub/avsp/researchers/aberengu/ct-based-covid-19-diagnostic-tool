import torch.nn as nn
from .module import LRPModule


class LRPSequential(LRPModule, nn.Sequential):

    def __init__(self):
        nn.Sequential.__init__(self)
        LRPModule.__init__(self)

    def forward(self, x, lrp_aware=False):
        if lrp_aware is True:
            self.input_cache = x

        for module in self:
            if lrp_aware is False:
                x = module(x)
            else:
                assert isinstance(module, LRPModule), 'LRP impossible: {} is not an instance of LRPModule'.format(
                    module.__class__.__name__)
                x = module.lrp_forward(x)
        return x

    def lrp(self, Rout, hout, lrp_type='simple'):

        if self.force_lrp_type is not None:
            lrp_type = self.force_lrp_type

        for i in range(len(self) - 1, -1, -1):
            if i == len(self) - 1:
                hout = hout
            else:
                hout = self[i + 1].get_input_cache()

            Rout = self[i].lrp(Rout, hout, lrp_type=lrp_type)

        return self.set_lrp_cache(Rout)
