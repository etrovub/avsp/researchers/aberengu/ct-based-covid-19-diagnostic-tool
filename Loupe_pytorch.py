##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#                   Boris Joukovsky (LRP compatibility)
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

""" 
This implementation is based on the works of [1,2] and the implementation in Tensorflow available at https://github.com/antoine77340/LOUPE
If you use this routine/program/software you also cite the following works:
[1] Arandjelovic, Relja, et al. "NetVLAD: CNN architecture for weakly supervised place recognition." Proceedings of the IEEE conference on computer vision and pattern recognition. 2016.
[2] Miech, Antoine, Ivan Laptev, and Josef Sivic. "Learnable pooling with context gating for video classification." arXiv preprint arXiv:1706.06905 (2017)
"""
import math
import torch
import torch.nn as nn
import torch.functional as F

def add_activation_layer(activation):
    if activation == 'relu':
        layer = nn.ReLU(inplace=True)
    elif activation == 'leakyrelu':
        layer = nn.LeakyReLU(inplace=True)
    elif activation == 'elu':
        layer = nn.ELU(inplace=True)
    elif activation == 'tanh':
        layer = nn.Tanh()
    elif activation == 'sigmoid':
        layer = nn.Sigmoid()
    elif activation == 'celu':
        layer = nn.CELU(inplace=True)
    elif activation == 'prelu':
        layer = nn.PReLU()
    return layer


def build_NN(dim_list, activation=None, batch_norm=True, dropout=0):
    layers = []
    for dim_in, dim_out in zip(dim_list[:-1], dim_list[1:]):
        layers.append(nn.Linear(dim_in, dim_out))
        if batch_norm:
            layers.append(nn.BatchNorm1d(dim_out))
        if activation is not None:
            layers.append(add_activation_layer(activation=activation))
        elif activation is None:
            print('non activation')
        if dropout > 0:
            layers.append(nn.Dropout(p=dropout))
    return nn.Sequential(*layers)

class NetVLAD(nn.Module):
    def __init__(self, feature_size, cluster_size, output_dim,
                 gating=True, add_batch_norm=True):
        super(NetVLAD, self).__init__()

        self.feature_size = feature_size
        self.cluster_size = cluster_size
        self.output_dim = output_dim
        self.gating = gating
        self.add_batch_norm = add_batch_norm

        self.softmax = nn.Softmax(dim=-1)

        self.cluster_weights = nn.Parameter(torch.randn(feature_size, cluster_size) * 1 / math.sqrt(feature_size))

        self.cluster_weights2 = nn.Parameter(torch.randn(1, feature_size, cluster_size) * 1 / math.sqrt(feature_size))

        self.hidden1_weights = nn.Parameter(
            torch.randn(cluster_size * feature_size, output_dim) * 1 / math.sqrt(feature_size))

        if add_batch_norm:
            self.cluster_biases = None
            self.bn1 = nn.BatchNorm1d(cluster_size)
        else:
            self.cluster_biases = nn.Parameter(torch.randn(cluster_size) * 1 / math.sqrt(feature_size))
            self.bn1 = None
        if gating:
            #self.context_gating = GatingContext(output_dim, add_batch_norm=add_batch_norm, dropout=0.5)
            self.context_gating = GatingContext(output_dim, add_batch_norm=add_batch_norm, dropout=0.0)
    def forward(self, x, max_samples=None):

        if max_samples is None:
            assert(max_samples is not None), 'VLAD: max_samples is not specified'
            max_samples = max_samples

        #x = x.view((1, max_samples, self.feature_size))
        x = x.view((-1, max_samples, self.feature_size))
        activation = torch.matmul(x, self.cluster_weights)
        if self.add_batch_norm:
            activation = activation.view(-1, self.cluster_size)
            activation = self.bn1(activation)
            activation = activation.view(-1, max_samples, self.cluster_size)
        else:
            activation = activation + self.cluster_biases
        activation = self.softmax(activation)
        activation = activation.view((-1, max_samples, self.cluster_size))

        a_sum = activation.sum(-2, keepdim=True)
        a = a_sum * self.cluster_weights2

        activation = torch.transpose(activation, 2, 1)
        x = x.view((-1, max_samples, self.feature_size))
        vlad = torch.matmul(activation, x)
        vlad = torch.transpose(vlad, 2, 1)
        vlad = vlad - a

        norm = F.norm(vlad, dim=1, p=2, keepdim=True)
        vlad = vlad.div(norm.expand_as(vlad))
        vlad = torch.reshape(vlad, [-1, self.cluster_size * self.feature_size])
        norm = F.norm(vlad, dim=1, p=2, keepdim=True)
        vlad = vlad.div(norm.expand_as(vlad))

        vlad = torch.matmul(vlad, self.hidden1_weights)

        if self.gating:
            vlad = self.context_gating(vlad)
        return vlad

class GatingContext(nn.Module):
    def __init__(self, dim, add_batch_norm=True, activation='leakyrelu', dropout=0.0):
        super(GatingContext, self).__init__()
        self.dim = dim

        if add_batch_norm:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=True, dropout=dropout)
        else:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=False, dropout=dropout)
        ###############################################
        # Initialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()

    def forward(self, x):

        gates = self.gate(x)

        activation = x * gates

        return activation
