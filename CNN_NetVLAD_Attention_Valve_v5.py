##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#                   Boris Joukovsky (LRP compatibility)
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB ETRO make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from Loupe_pytorch import NetVLAD as NetVLAD
import math

from modules import build_Convd2d, build_NN, SPP
from modules_pytorch import pooling, linear
from modules_pytorch.cnn_netvlad import spp, vlad
from modules_pytorch.module import LRPModule
from modules_pytorch.utils import transfer_sequential, transfer_linear

def add_activation_layer(activation):
    if activation == 'relu':
        layer = nn.ReLU(inplace=True)
    elif activation == 'leakyrelu':
        layer = nn.LeakyReLU(inplace=True)
    elif activation == 'elu':
        layer = nn.ELU(inplace=True)
    elif activation == 'tanh':
        layer = nn.Tanh()
    elif activation == 'sigmoid':
        layer = nn.Sigmoid()
    elif activation == 'celu':
        layer = nn.CELU(inplace=True)
    elif activation == 'prelu':
        layer = nn.PReLU()
    return layer

def build_Convd2d(in_channels, out_channels, kernel_size, stride, padding, activation, add_batch_norm=True):
    if add_batch_norm:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            nn.BatchNorm2d(out_channels),
            add_activation_layer(activation=activation)
        )
    else:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            add_activation_layer(activation=activation)
        )
def build_NN(dim_list, activation=None, batch_norm=True, dropout=0):
    layers = []
    for dim_in, dim_out in zip(dim_list[:-1], dim_list[1:]):
        layers.append(nn.Linear(dim_in, dim_out))
        if batch_norm:
            layers.append(nn.BatchNorm1d(dim_out))
        if activation is not None:
            layers.append(add_activation_layer(activation=activation))
        elif activation is None:
            print('non activation')
        if dropout > 0:
            layers.append(nn.Dropout(p=dropout))
    return nn.Sequential(*layers)

class SPP(nn.Module):
    def __init__(self, level_pool_size, pool_operation='max'):
        super(SPP, self).__init__()
        self.level_pool_size = level_pool_size

        if pool_operation=='max':
            self.pool_operation = pool_operation
        else:
            raise ValueError(
                "This pooling operation is not defined {}...\n".format(pool_operation))

    def forward(self, input):
        if len(input.size()) < 4:
            raise ValueError(
                "Expected 4D input size but got { }D...\n".format(len(input.size())))

        if input.size()[-2] < self.level_pool_size[0] or input.size()[-1] < self.level_pool_size[0]:
            raise ValueError(
                "Pyramid pooling size can not be higher than previous filter size{}...\n".format(self.pool_operation))
        bath_size = input.size()[0]
        h, w = input.size()[-2:]

        for i in range(len(self.level_pool_size)):

            h_wid = math.ceil(h / self.level_pool_size[i])
            w_wid = math.ceil(w / self.level_pool_size[i])
            h_str = math.floor(h / self.level_pool_size[i])
            w_str = math.floor(w / self.level_pool_size[i])
            if self.pool_operation == 'max':
                x= F._max_pool2d(input,kernel_size=(h_wid, w_wid), stride=(h_str, w_str))
            # elif self.pool_operation == 'mean':
            #     x = nn.AdaptiveAvgPool2d(kernel_size=(h_wid, w_wid), stride=(h_str, w_str))
            if i == 0:
                spp = x.view(bath_size, -1)
            else:
                spp = torch.cat((spp, x.view(bath_size, -1)), 1)
        return spp

class GatingValve(nn.Module):
    def __init__(self, dim, activation='sigmoid', add_batch_norm=True):
        super(GatingValve, self).__init__()
        self.dim = dim
        if add_batch_norm:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=True)
        else:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=False)
        ###############################################
        # Reinitialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()

    def forward(self, x, y):

        attention = torch.mul(x, y)

        attention = attention.view(-1, attention.size()[1] * attention.size()[2] * attention.size()[3])

        gated_content = self.gate(attention)

        #gated_content = gated_content * x.view(-1, x.size()[1] * x.size()[2] * x.size()[3])

        return gated_content

##########################
### MODEL
##########################

class CNN_NetVLAD(torch.nn.Module, LRPModule):
    def __init__(self,num_classes):
        torch.nn.Module.__init__(self)
        LRPModule.__init__(self)

        self.num_classes = num_classes

        self.conv_1 = build_Convd2d(in_channels=1, out_channels=16, kernel_size=(5,5), stride=(2,2), padding=0, activation='leakyrelu', add_batch_norm=False)

        self.pool_1 = nn.MaxPool2d(kernel_size=(2,2),stride=(2,2),padding=0)

        self.conv_2 = build_Convd2d(in_channels=16, out_channels=32, kernel_size=(3, 3), stride=(2, 2), padding=0, activation='leakyrelu', add_batch_norm=False)

        self.pool_2 = nn.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)

        self.conv_3 = build_Convd2d(in_channels=32, out_channels=64, kernel_size=(3, 3), stride=(2, 2), padding=0, activation='leakyrelu', add_batch_norm=False)

        #self.pool_3 = nn.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)

        self.conv_1_valve = build_Convd2d(in_channels=1, out_channels=16, kernel_size=(5, 5), stride=(2, 2), padding=0,
                                    activation='leakyrelu', add_batch_norm=False)

        self.conv_2_valve = build_Convd2d(in_channels=16, out_channels=32, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                    activation='leakyrelu', add_batch_norm=False)

        self.conv_3_valve = build_Convd2d(in_channels=32, out_channels=64, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                    activation='leakyrelu', add_batch_norm=False)

        #self.valve=GatingValve(dim=(64 * 7 * 7),activation='relu', add_batch_norm=False)

        #self.norm_layer = nn.LayerNorm([64, 7, 7])

        #self.fc_1 = build_NN([64 * 7 * 7, 128], activation='relu')

        self.spp = SPP(level_pool_size=[5,3,2],pool_operation='max')

        self.fc_1 = build_NN([3136, 512], activation='leakyrelu', batch_norm=False, dropout=0.85)

        self.VLAD = NetVLAD(feature_size=512, cluster_size=64, output_dim=512, gating=True, add_batch_norm=False)

        self.fc_2 = nn.Linear(512,self.num_classes)

        #self.out = nn.Softmax()

        ###############################################
        # Reinitialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()
            elif isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()

    def convert_to_lrp_model(self):
        # Converting layers to lrp_aware layers
        self.conv_1 = transfer_sequential(self.conv_1)
        self.pool_1 = pooling.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)
        self.conv_2 = transfer_sequential(self.conv_2)
        self.pool_2 = pooling.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)
        self.conv_3 = transfer_sequential(self.conv_3)
        # self.pool_3 = pooling.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)

        self.conv_1_valve = transfer_sequential(self.conv_1_valve)
        self.pool_1_valve = pooling.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)
        self.conv_2_valve = transfer_sequential(self.conv_2_valve)
        self.pool_2_valve = pooling.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)
        self.conv_3_valve = transfer_sequential(self.conv_3_valve)
        # self.pool_3_valve = pooling.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)

        self.spp = spp.SPP(self.spp.level_pool_size, self.spp.pool_operation)

        self.fc_1 = transfer_sequential(self.fc_1)

        self.VLAD = vlad.NetVLAD(base_vlad = self.VLAD)

        self.fc_2 = transfer_linear(self.fc_2)

    def forward(self, input_data, input_lesions, max_samples, lrp_aware=False):

        if lrp_aware:
            self.input_cache_data = input_data
            self.input_cache_lesions = input_lesions

        if lrp_aware is False:
            x = self.conv_1(input_data)
            x = self.pool_1(x)
        else:
            x = self.conv_1(input_data, lrp_aware)
            x = self.pool_1.lrp_forward(x)
        #print('conv1 out:', x.size())

        if lrp_aware is False:
            x = self.conv_2(x)
            x = self.pool_2(x)
        else:
            x = self.conv_2(x, lrp_aware)
            x = self.pool_2.lrp_forward(x)
        #print('conv2 out:', x.size())

        if lrp_aware is False:
            x = self.conv_3(x)
        else:
            x = self.conv_3(x, lrp_aware)
        #print('conv3 out:', x.size())


        if lrp_aware is False:
            y = self.conv_1_valve(input_lesions)
            y = self.pool_1(y)
        else:
            y = self.conv_1_valve(input_lesions, lrp_aware)
            y = self.pool_1_valve.lrp_forward(y)
        #print('conv1_valve out:', y.size())

        if lrp_aware is False:
            y = self.conv_2_valve(y)
            y = self.pool_2(y)
        else:
            y = self.conv_2_valve(y, lrp_aware)
            y = self.pool_2_valve.lrp_forward(y)
        #print('conv2_valve out:', y.size())

        if lrp_aware is False:
            y = self.conv_3_valve(y)
        else:
            y = self.conv_3_valve(y, lrp_aware)
        #print('conv3_valve_out:', y.size())

        #y = y.repeat(1, x.size()[1], 1, 1)

        self.x_gate = x
        self.y_gate = y
        gated = torch.mul(x,y)

        x = F.relu(gated, inplace=True)

        #gated = F.relu(gated)

        #gated = x + y

        #x = self.valve(x,y)

        # x = x.view(-1,64,7,7)

        if lrp_aware is False:
            x = self.spp(x)
        else:
            x = self.spp.lrp_forward(x)

        # x = x.view(-1, x.size()[1] * x.size()[2] * x.size()[3])

        if lrp_aware is False:
            x = self.fc_1(x)
        else:
            x = self.fc_1(x, lrp_aware)
        #print('fc_1 out:', x.size())

        self.max_samples = max_samples
        if lrp_aware is False:
            x = self.VLAD(x, max_samples)
        else:
            x = self.VLAD.lrp_forward(x, max_samples)
        #print('VLAD out:', x.size())

        if lrp_aware is False:
            logits = self.fc_2(x)
        else:
            logits = self.fc_2.lrp_forward(x)

        #print('fc_2 out:', logits.size())

        proba = F.softmax(logits, dim=1)

        #print('output out:', logits.size())

        return logits, proba

    def lrp(self, Ry, y, lrp_classifier='simple', lrp_feat='gamma', lrp_pool = 'simple'):

        #TODO: Better class discrimintation

        R = self.fc_2.lrp(Ry, y, lrp_classifier)

        R = self.VLAD.lrp(R, y, 'simple')[0,...]

        R = self.fc_1.lrp(R, self.VLAD.get_input_cache().data, lrp_type='simple')

        R = self.spp.lrp(R, self.fc_1.get_input_cache(), lrp_type='simple')

        # View: Nothing changes

        # R_gated = R.detach().cpu()

        # Gate: Just use GxI for now
        R_sum = R.sum().item()
        x_x = torch.nn.Parameter(self.x_gate.detach().data, requires_grad=True)
        x_y = torch.nn.Parameter(self.y_gate.detach().data, requires_grad=True)
        y = torch.mul(x_x,x_y)
        y = torch.nn.functional.relu(y, inplace=True)
        y.backward(gradient=R.data)
        R_lung = x_x.grad * x_x
        R_lesions = x_y.grad * x_y
        Rsum = R_lung.sum() + R_lesions.sum()
        R_lung = R_lung/Rsum * R_sum
        R_lesions = R_lesions/Rsum * R_sum


        # Lungs
        R_lung = self.conv_3.lrp(R_lung, x_x, lrp_type=lrp_feat)
        R_lung = self.pool_2.lrp(R_lung, self.conv_3.get_input_cache(), lrp_type='simple')
        R_lung = self.conv_2.lrp(R_lung, self.pool_2.get_input_cache(), lrp_type=lrp_feat)
        R_lung = self.pool_1.lrp(R_lung, self.conv_2.get_input_cache(), lrp_type='simple')
        R_lung = self.conv_1.lrp(R_lung, self.pool_1.get_input_cache(), lrp_type='zb')

        # Lesions
        R_lesions = self.conv_3_valve.lrp(R_lesions, x_y, lrp_type=lrp_feat)
        R_lesions = self.pool_2_valve.lrp(R_lesions, self.conv_3_valve.get_input_cache(), lrp_type='simple')
        R_lesions = self.conv_2_valve.lrp(R_lesions, self.pool_2_valve.get_input_cache(), lrp_type=lrp_feat)
        R_lesions = self.pool_1_valve.lrp(R_lesions, self.conv_2_valve.get_input_cache(), lrp_type='simple')
        R_lesions = self.conv_1_valve.lrp(R_lesions, self.pool_1_valve.get_input_cache(), lrp_type='zb')

        return R_lung, R_lesions