##########################################################################################
# (c) Copyright 2020
# The author(s):    Boris Joukovsky (LRP compatibility)
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

import cv2
import os
import numpy as np
import SimpleITK as sitk
from scipy.ndimage.filters import gaussian_filter


def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")
    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)


def overlay(img, overlay, normfac=None, gamma=2.5):
    if normfac is None:
        normfac = max(abs(overlay.min()), overlay.max())

    op = np.where(overlay > 0.0, overlay, 0)
    op = op / normfac * 255.0
    op[op > 255.0] = 255.0

    # on = np.where(overlay < 0.0, overlay, 0) * -1.0
    # on = on / normfac * 180.0
    # on[on > 180.0] = 180.0

    op = adjust_gamma(op.astype('uint8'), gamma)
    op = cv2.applyColorMap(op, cv2.COLORMAP_JET)
    # on = cv2.applyColorMap(on.astype('uint8'), cv2.COLORMAP_OCEAN)
    # on = adjust_gamma(on)

    # out = cv2.addWeighted(cv2.addWeighted(img, 0.5, op, 1.0, 0.0), 1.0, on, 1.0, 0.0)
    out = cv2.addWeighted(img, 0.7, op, 0.55, 0.0)
    return out


def output_sitk(slices, relevance, path, filename, normfac, metadata, noscan=False):
    assert (slices.shape[1] == 1), 'Error: Please use a batch size of 1'

    if os.path.exists(path) is False:
        os.makedirs(path)

    slices = slices.detach().cpu().numpy()

    sp, sn, s = np.zeros(slices.shape[0]), np.zeros(slices.shape[0]), np.zeros(slices.shape[0])

    lungs_3d_image = np.zeros((slices.shape[0], slices.shape[2], slices.shape[3], 3), dtype='uint8')
    relevance_3d_image = np.zeros((slices.shape[0], slices.shape[2], slices.shape[3]))
    relevance_3d_image_raw = np.zeros((slices.shape[0], slices.shape[2], slices.shape[3]))

    for s_i in range(slices.shape[0]):
        s_img = slices[s_i, 0, ...]
        s_img = s_img * 255.0
        s_img = cv2.cvtColor(s_img.astype('uint8'), cv2.COLOR_GRAY2RGB)

        lungs_3d_image[s_i, ...] = s_img

        R_img = relevance[s_i, 0, ...]

        R_OUT = np.where(R_img > 0.0, R_img, 0)
        r_out = gaussian_filter(R_OUT, 4)
        r_out = r_out / normfac * 255.0
        r_out[r_out > 255.0] = 255.0
        r_out = adjust_gamma(r_out.astype('uint8'), 1.0)

        R_OUT = R_OUT / normfac * 255.0
        R_OUT[R_OUT > 255.0] = 255.0
        R_OUT = adjust_gamma(R_OUT.astype('uint8'), 1.0)

        relevance_3d_image[s_i, ...] = r_out
        relevance_3d_image_raw[s_i, ...] = R_OUT

        sp[s_i] = np.where(R_img > 0.0, R_img, 0.0).sum()
        sn[s_i] = np.where(R_img < 0.0, R_img, 0.0).sum()
        s[s_i] = R_img.sum()

    if noscan is False:
        save_3d_volume(path, filename+'.mha', lungs_3d_image, metadata)
    save_3d_volume(path, filename+'_relevance.mha', relevance_3d_image, metadata)


def save_3d_volume(dir, filename, volume, metadata):

    filepath = os.path.join(dir, filename)

    vol = sitk.GetImageFromArray(volume)

    if metadata is not None:
        vol.SetOrigin(metadata[0])
        vol.SetDirection(metadata[1])
        vol.SetSpacing(metadata[2])
    else:
        print('Warning: Continuing without metadata for 3D volume. Scales and orientations might be incorrect.')
    sitk.WriteImage(vol, filepath)

