##########################################################################################
# (c) Copyright 2020
# The author(s):    Maryna Kvasnytsia
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

import os
import SimpleITK as sitk

def save_heatmap(input_image_fname, heatmap_nda, output_dir):
    
    """
    Inputs:
           heatmap_nda is a numpy array of a heatmap
           input_image_fname is a filename of the original input CT image
           output_dir is a directory to save heatmap files
    """
    # First we load input image to access its meta information
    input_image = sitk.ReadImage(input_image_fname)
    origin = input_image.GetOrigin()
    spacing = input_image.GetSpacing()
    direction = input_image.GetDirection()
    
    # convert numpy array to sitk image
    img = sitk.GetImageFromArray(heatmap_nda)
    # heatmap image inherits some metadata from  original one so they could be aligned
    img.SetOrigin(origin)
    img.SetDirection(direction)
    img.SetSpacing(spacing)
    # save heatmap image
    heatmap_filename = os.path.join(output_dir, 'heatmap.nii')
    sitk.WriteImage(img, heatmap_filename)

    return


