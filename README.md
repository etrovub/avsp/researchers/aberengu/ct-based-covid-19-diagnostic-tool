# CT-Based COVID-19 Diagnostic 

This project contains the code of the CT-Based COVID-19 Diagnostic System.

### Authors
- Abel Díaz Berenguer (aberengu@etrovub.be)
- Boris Joukovsky (bjoukovs@etrovub.be)
- Maryna Kvasnytsia (mkvasnyt@etrovub.be)
- Ine Dirks (idirks@etrovub.be)

## Installation
- Install the required packages specified in `requirements.txt`. Other versions of the required packages might work but are untested.
- Download the pretrained model, change the .sh files to set the checkpoint folder path.

Note: By default the pretrained model is inside the folder ./Model

## Inputs
- To test the code as-is, three nifti (.nii) files per each patient must be provided:

1.  The lung CT-scan (ct.nii.gz)
2.  The lung mask resulting form segmentation (ct_mask.nii.gz)
3.  The binary lesion segmentation masks resulting from lesion segmentation(ct_lesions.nii.gz)
4.  Change the .sh files with the path to the root folder were these files are located.

By default, it was considered:

1.  These three files are located inside the folder ./Processed_data
2.  The filename as specified between brackets.

## Test
Run `test.sh`
## Interpretability maps
Run  `explain.sh` 

The outputs of the explainability will be saved by default in the folder: ./Processed_data/output

To have a similar setup to the figures in the paper, use Slicer and setup 3 views:

- 1 with the original lung scan (./Processed_data/ct.nii.gz)
- 1 with the segmented lung and heatmap overlay (C_lungs.mha and C_lungs_relevance.mha files)
- 1 with the lesions and heatmap overlay (C_lesion.mha, C_lesions_relevance.mha files)

For the overlays, you can use the ColdToHotRainbow colormap

Do not forget to setup the color range for the heatmaps correctly ! (Select 'manual min/max' and choose from 0 to 255, you can reduce to 200 for details)

Note: This code has not been tested on CPU only (Some Tensors are initialized on GPU by default).

## How to cite
If you use this code in your work, please cite [Explainable-by-design Semi-Supervised Representation Learning for COVID-19 Diagnosis from CT Imaging](https://arxiv.org/pdf/2011.11719)
