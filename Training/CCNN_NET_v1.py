# -*- coding: utf-8 -*-
##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#                   Boris Joukovsky (LRP compatibility)
#                   Maryna Kvasnytsia
#                   Ine Dirks
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

            #######################################################################################################
            ###This is an ongoing research project, please do not use this program for autonomous self-diagnosis###
            #######################################################################################################

import torch
import torch.nn as nn
import torch.nn.functional as F

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Device:', device)

def add_activation_layer(activation):
    if activation == 'relu':
        layer = nn.ReLU(inplace=True)
    elif activation == 'leakyrelu':
        layer = nn.LeakyReLU(inplace=True)
    elif activation == 'elu':
        layer = nn.ELU(inplace=True)
    elif activation == 'tanh':
        layer = nn.Tanh()
    elif activation == 'sigmoid':
        layer = nn.Sigmoid()
    elif activation == 'celu':
        layer = nn.CELU(inplace=True)
    elif activation == 'prelu':
        layer = nn.PReLU()
    return layer

def build_deConvd2d(in_channels, out_channels, kernel_size, stride, padding, activation=None):
        if activation!=None:
            return torch.nn.Sequential(
                nn.ConvTranspose2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                                              stride=stride, padding=padding), add_activation_layer(activation=activation))
        else:
            return torch.nn.Sequential(
                nn.ConvTranspose2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding))


def build_Convd2d(in_channels, out_channels, kernel_size, stride, padding, activation=None):
    if activation != None:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            add_activation_layer(activation=activation))
    else:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding))

def upSample(scale_factor):
    return torch.nn.Sequential(
       nn.Upsample(scale_factor=scale_factor, mode='bilinear', align_corners=True)
    )

def build_NN(dim_list, activation=None, batch_norm=False, dropout=0.0):
    layers = []
    for dim_in, dim_out in zip(dim_list[:-1], dim_list[1:]):
        layers.append(nn.Linear(dim_in, dim_out))
        if batch_norm:
            layers.append(nn.BatchNorm1d(dim_out))
        if activation is not None:
            layers.append(add_activation_layer(activation=activation))
        elif activation is None:
            print('non activation')
        if dropout > 0:
            layers.append(nn.Dropout(p=dropout))
    return nn.Sequential(*layers)

class GatingContext(nn.Module):

    def __init__(self, dim_input, dim_output, activation='leakyrelu'):
        super(GatingContext, self).__init__()
        self.dim_input = dim_input
        self.dim_output = dim_output
        self.merge = build_NN([dim_input, dim_output], activation=activation)
        self.gate = build_NN([dim_output, dim_output],activation=activation)

        ###############################################
        # Reinitialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()
            elif isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()

    def forward(self, x, y):

        input_merge = torch.cat((x, y),dim=1)

        content = self.merge(input_merge)

        gated = self.gate(content)

        gated_content = gated * content

        return gated_content

##########################
### MODEL
##########################

class ConditionalVariationalAutoencoder(torch.nn.Module):

    def __init__(self, num_latent, num_classes, label_embedding_dim, context_gating_ouput_dim):
        super(ConditionalVariationalAutoencoder, self).__init__()

        self.num_latent = num_latent
        self.num_classes = num_classes
        self.label_embedding_dim = label_embedding_dim
        self.context_gating_ouput_dim = context_gating_ouput_dim

        ###############
        # ENCODER
        ##############
        self.conv_1 = build_Convd2d(in_channels=1, out_channels=16, kernel_size=(5,5), stride=(2,2), padding=0,
                                    activation='leakyrelu')

        self.pool_1 = nn.MaxPool2d(kernel_size=(2,2),stride=(2,2),padding=0)

        self.conv_2 = build_Convd2d(in_channels=16, out_channels=32, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                    activation='leakyrelu')

        self.pool_2 = nn.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)

        self.conv_3 = build_Convd2d(in_channels=32, out_channels=64, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                    activation='leakyrelu')

        self.pool_3 = nn.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)

        self.conv_1_valve = build_Convd2d(in_channels=1, out_channels=16, kernel_size=(5, 5), stride=(2, 2), padding=0,
                                          activation='leakyrelu')

        self.conv_2_valve = build_Convd2d(in_channels=16, out_channels=32, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                          activation='leakyrelu')

        self.conv_3_valve = build_Convd2d(in_channels=32, out_channels=64, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                          activation='leakyrelu')

        self.label_cond = build_NN([self.num_classes, self.label_embedding_dim],activation='leakyrelu')

        self.gate_enc = GatingContext(dim_input=(64 * 14 * 14)+self.label_embedding_dim,dim_output=self.context_gating_ouput_dim,activation='leakyrelu')

        self.phi_mu = nn.Linear(self.context_gating_ouput_dim, self.num_latent)

        self.phi_sigma = nn.Linear(self.context_gating_ouput_dim, self.num_latent)

        ###############
        # DECODER
        ##############
        self.dec_linear = nn.Linear(num_latent, (64 * 14 * 14))

        self.dec_deconv_1 = build_deConvd2d(in_channels=64, out_channels=32, kernel_size=(3, 3), stride=(2, 2),
                                            padding=0, activation='leakyrelu')

        self.upSample_1 = upSample(scale_factor=1.6)

        self.dec_deconv_2 = build_deConvd2d(in_channels=32, out_channels=16, kernel_size=(3, 3), stride=(2, 2),
                                            padding=0, activation='leakyrelu')

        self.upSample_2 = upSample(scale_factor=1.6)

        self.dec_deconv_3 = build_deConvd2d(in_channels=16, out_channels=1, kernel_size=(2, 2), stride=(2, 2),
                                            padding=0, activation='leakyrelu')

        self.upSample_3 = upSample(scale_factor=1.595)

        ###############################################
        # Reinitialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()
            elif isinstance(m, torch.nn.ConvTranspose2d):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()
            elif isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()

    def reparameterize(self, mu, sigma):
        if self.training:
            std = torch.exp(sigma / 2.).to(device)
            eps = torch.randn_like(mu).to(device)
            return eps.mul(std).add_(mu)
        else:
            return mu

    def encoder_decoder(self, input_data, input_lesions,labels):

        x = self.conv_1(input_data)

        x = self.pool_1(x)

        x = self.conv_2(x)

        x = self.pool_2(x)

        x = self.conv_3(x)

        y = self.conv_1_valve(input_lesions)

        y = self.pool_1(y)

        y = self.conv_2_valve(y)

        y = self.pool_2(y)

        y = self.conv_3_valve(y)

        l_cond = self.label_cond(labels)

        ###############
        # Equation (2) # Valve filter with side information
        ##############

        x = torch.mul(x, y)

        x = F.relu(x)

        x = x.view(-1, 64 * 14 * 14)

        ###############
        # Equation (4,5) # Context Gating
        ##############
        gated_cond = self.gate_enc(x, l_cond)

        ###############
        # Equation (1)
        ##############
        z_mu = self.phi_mu(gated_cond)
        z_sigma = self.phi_sigma(gated_cond)
        encoded = self.reparameterize(z_mu, z_sigma)

        x = self.dec_linear(encoded)

        x = x.view(-1, 64, 14, 14)

        x = self.dec_deconv_1(x)

        x = self.upSample_1(x)

        x = self.dec_deconv_2(x)

        x = self.upSample_2(x)

        x = self.dec_deconv_3(x)

        x = self.upSample_3(x)

        decoded = torch.sigmoid(x)
        return z_mu, z_sigma, encoded, decoded

    def forward(self, slices, lesions, labels):
        z_mu, z_sigma, encoded, decoded = self.encoder_decoder(slices, lesions, labels)
        return z_mu, z_sigma, encoded, decoded