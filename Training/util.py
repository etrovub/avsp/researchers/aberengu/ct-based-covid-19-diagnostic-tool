#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 14:52:46 2020
Helper functions for preprocessing

@authors: marynak & aberengu
"""
import SimpleITK as sitk
import numpy as np
import sys
import os

#############################################
# Auxiliary functions to preprocess the data#
#############################################

#  dict explains labels in the lesion segmentation mask
LABELS = {
    # Basic lesion classes
    'ggo': 1,
    'consolidation': 2,
    'crazy_paving_pattern': 3,
    # 'linear_opacity': 2,  # not needed for inference
    # Super classes
    'combined_pattern': 4,
    # 'reversed_halo_sign': 4,  # not needed for inference
    'other_abnormal_tissue': 5,
    'lungs': 6,
    'background': 0,
}

def binarize_lesions_image(image):
    '''
    Binarizes multi-label mask of lesions,
    labeled according to the LABELS dict
    Output is binary image with lesions labeled 1
    '''

    max_lesions_class = np.max([LABELS['ggo'], LABELS['consolidation'],
                                LABELS['crazy_paving_pattern'],
                                LABELS['combined_pattern']])
    img_array = sitk.GetArrayFromImage(image)
    img_array[img_array > max_lesions_class] = 0
    img_array[(img_array < max_lesions_class) & (img_array > 0)] = 1
    binary_image = sitk.GetImageFromArray(img_array)

    origin = image.GetOrigin()
    spacing = image.GetSpacing()
    direction = image.GetDirection()

    binary_image.SetOrigin(origin)
    binary_image.SetDirection(direction)
    binary_image.SetSpacing(spacing)

    return binary_image

def binarize_lungs_mask(image):
    '''
    binarizes lung mask.
    Inputs:
        image is a lung mask image,
        it is expected that it has [0,1]
        range of intensity values

    '''

    thresholder = sitk.BinaryThresholdImageFilter()
    thresholder.SetLowerThreshold(0.5)
    thresholder.SetUpperThreshold(1)
    thresholder.SetOutsideValue(0)
    thresholder.SetInsideValue(1)
    binary_image = thresholder.Execute(image)

    return binary_image

def binarize_image(image):#same as binarize_lungs_mask
    print('Binarizing image...')
    thresholder = sitk.BinaryThresholdImageFilter()
    thresholder.SetLowerThreshold(0.5)
    thresholder.SetUpperThreshold(1)
    thresholder.SetOutsideValue(0)
    thresholder.SetInsideValue(1)
    binary_image = thresholder.Execute(image)
    return binary_image

def normalize(image_array):
    print('Normalizing image...')
    min_bound = -1000.0
    max_bound = 400.0
    image_array = (image_array - min_bound) / (max_bound - min_bound)
    image_array[image_array > 1] = 1.
    image_array[image_array < 0] = 0.
    return image_array


def create_data(input_data_root, number_of_slices, covid_status=0): #### This is not well-optimized

    print('Loading data, this may take a few minutes...\n')

    path_to_original_ct_scanfile = os.path.join(input_data_root,'ct.nii.gz')

    path_to_lung_mask = os.path.join(input_data_root,'ct_mask.nii.gz')

    path_to_lesions_segmentation = os.path.join(input_data_root,'ct_lesions.nii.gz')

    assert (os.path.exists(path_to_original_ct_scanfile) is True), 'This file {} .... does not exist...\n'.format(path_to_original_ct_scanfile)

    assert (os.path.exists(path_to_original_ct_scanfile) is True), 'This file {} .... does not exist...\n'.format(
        path_to_lung_mask)

    assert (os.path.exists(path_to_original_ct_scanfile) is True), 'This file {} .... does not exist...\n'.format(
        path_to_lesions_segmentation)

    image_ct = sitk.ReadImage(path_to_original_ct_scanfile)  # get ct_image#

    image_lung_mask = sitk.ReadImage(path_to_lung_mask)  #get lungs mask#

    image_lesion_mask = sitk.ReadImage(path_to_lesions_segmentation)  # get ct_image#

    if image_ct.GetSize() != image_lung_mask.GetSize() != image_lesion_mask.GetSize():
        print('Equal sizes are required....')

        print('Size image is: {}'.format(image_ct.GetSize()))

        print('Size mask is: {}'.format(image_lesion_mask.GetSize()))

        print('Size lesion is: {}'.format(image_lesion_mask.GetSize()))

        sys.exit()

    if image_ct.GetSize() != image_lung_mask.GetSize() != image_lesion_mask.GetSize():
        print('Equal sizes are required....')

        print('Size image is: {}'.format(image_ct.GetSize()))

        print('Size mask is: {}'.format(image_lesion_mask.GetSize()))

        print('Size lesion is: {}'.format(image_lesion_mask.GetSize()))

        sys.exit()

    if image_ct.GetDimension() < 3 or image_lung_mask.GetDimension() < 3 or image_lesion_mask.GetDimension() < 3:
        print('Equal sizes are required....')

        print('DIM image is: {}'.format(image_ct.GetDimension()))

        print('DIM mask is: {}'.format(image_lesion_mask.GetDimension()))

        print('DIM lesion is: {}'.format(image_lesion_mask.GetDimension()))

        sys.exit()

    elif image_ct.GetWidth() != image_ct.GetHeight() or (image_ct.GetWidth() < 512): #### Could be lower than
                                                                                     #### 512  but we assumed these dimensions
        print('Square Slices of with dim equal are required....')

        print('Width is: {}'.format(image_ct.GetWidth()))

        print('Height is: {}'.format(image_ct.GetHeight()))

        sys.exit()


    ct_array = sitk.GetArrayFromImage(image_ct)[:, 20:492, 20:492]

    lung_mask_array = sitk.GetArrayFromImage(binarize_image(image_lung_mask))[:, 20:492, 20:492]

    lesions_array = sitk.GetArrayFromImage(binarize_image(image_lesion_mask))[:, 20:492, 20:492]

    #It depends on the outupts of the lesion_segmentation
    idx_zero = np.where(lesions_array == 0)
    idx_one = np.where(lesions_array == 1)

    lesions_array[idx_zero] = 1
    lesions_array[idx_one] = 0

    norm_ct_array = normalize(ct_array)

    masked_ct_array = norm_ct_array * lung_mask_array

    max_value_z = number_of_slices

    if ct_array.shape[0] > max_value_z:

        idx = np.arange(start=np.random.randint(0, (ct_array.shape[0] / max_value_z)),
                        step=(ct_array.shape[0] / max_value_z),
                        stop=ct_array.shape[0]).astype(int)

        if idx.shape[0] > max_value_z:

           idx = idx[:max_value_z]

        data = masked_ct_array[idx, :, :]

        data_lesions = lesions_array[idx, :, :]

    else:

        data = masked_ct_array[:, :, :]

        data_lesions = lesions_array[:, :, :]

    data = np.expand_dims(data, axis=0)
    data = np.expand_dims(data, axis=2)

    data_lesions = np.expand_dims(data_lesions, axis=0)
    data_lesions = np.expand_dims(data_lesions, axis=2)

    labels_data = np.zeros_like([0])
    labels_data[0] = covid_status

    return data, data_lesions, labels_data
