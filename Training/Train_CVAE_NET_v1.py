# -*- coding: utf-8 -*-
##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

            #######################################################################################################
            ###This is an ongoing research project, please do not use this program for autonomous self-diagnosis###
            #######################################################################################################

import argparse
import numpy as np
import torch
import time
import os
import errno
from Training.CCNN_NET_v1 import ConditionalVariationalAutoencoder
import random
import sys
import cv2
from torch.utils.data import Dataset
import torch.nn.functional as F

##########################
### SETTINGS
##########################
parser = argparse.ArgumentParser()
RANDOM_SEED = 0
##########################
# Data
##########################
parser.add_argument('--input_data_root', metavar='input_data', type=str, default='./',
                    help='Path to the root folder with the training data in .npy array')

parser.add_argument('--is_training', metavar='is_training', type=bool,
                    default=True,
                    help='Define if the model is in training or evaluations mode')
##########################
# Model Optimization
##########################
parser.add_argument('--num_epochs', metavar='epochs', type=int,
                    default=200,
                    help='Define if the number of epochs to train')

parser.add_argument('--learning_rate', metavar='learning_rate', type=float, help='Define the learning rate',
                    default=1e-5)

parser.add_argument('--beta', metavar='beta-variational', type=float, help='beta>>1 should led to more disentangled rep',
                    default=1.0)

parser.add_argument('--checkpoint_output', metavar='checkpoint_output', type=str,
                    default='./',
                    help='Path to the root folder to save the checkpoint')

parser.add_argument('--epoch', metavar='epoch', type=int,
                    default=0,
                    help='best_checkpoint to load')

parser.add_argument('--rec_output', metavar='folder_to_save', type=str,
                    default='./{}/{}',
                    help='Path to the root folder to save the checkpoint')

parser.add_argument('--batchsize', type=int, default=2,
                    help="Number of volumes to process simultaneously. Lower number requires less memory but may be slower")

parser.add_argument('--check_every', type=int, default=10,
                    help="amount of epoch to perform validation")

parser.add_argument('--num_epochs_train_early_stop', metavar='num_epochs_train_early_stop', type=int,
                    default=50, help='Number of epochs to stop if non improvements regarding the train loss')

parser.add_argument('--num_epochs_val_early_stop', metavar='num_epochs_val_early_stop', type=int,
                    default=50, help='Number of epochs to stop if non improvements regarding the validation loss')

##########################
# Model Parameters
##########################
parser.add_argument('--latent_dim', type=int,
                    help="Dimension of the latent representattion",
                    default=16)##64

parser.add_argument('--label_embedding_dim', type=int,
                    help="Dimension of the label embedding",
                    default=64)

parser.add_argument('--number_classes', type=int,
                    help="Number of classes",
                    default=2)

parser.add_argument('--context_gating_ouput_dim', type=int,
                    help="Dimension of the context gating mechanism",
                    default=2)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Device:', device)

##########################
# Data Loader
##########################
class Data(Dataset):
    def __init__(self, data_set, data_lesions,labels):
        super(Data, self).__init__()
        self.data_set = data_set
        self.data_lesions = data_lesions
        self.labels = labels

    def __len__(self):
        return len(self.data_set)

    def __getitem__(self, idx):
        return [self.data_set[idx, :, :, :].astype(float), self.data_lesions[idx, :, :, :].astype(float),
                self.labels[idx].astype(int)]

##########################
# Load train and validation data
##########################
def load_data(args,model):

    if model.training:
        print('Loading training data...\n')
        train_data = np.load(os.path.join(args.input_data_root, 'train/train_data.npy'), allow_pickle=True)
        print('Loading training data lesions...\n')
        train_data_lesions_masks = np.load(os.path.join(args.input_data_root, 'train/train_data_lesions.npy'),
                                         allow_pickle=True)
        print('Loading training labels...\n')
        train_labels = np.load(os.path.join(args.input_data_root, 'train/train_labels.npy'), allow_pickle=True)

        train_dataset = Data(train_data, train_data_lesions_masks, train_labels)

        train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batchsize, shuffle=True, num_workers=4,
                                                   pin_memory=True)

        print('Loading validation data...\n')
        val_data = np.load(os.path.join(args.input_data_root,'val/val_data.npy'), allow_pickle=True)
        print('Loading validation data lesions...\n')
        val_data_lesions_masks = np.load(os.path.join(args.input_data_root,'val/val_data_lesions.npy'), allow_pickle=True)
        print('Loading validation labels...\n')
        val_labels = np.load(os.path.join(args.input_data_root,'val/val_labels.npy'), allow_pickle=True)

        val_dataset = Data(val_data, val_data_lesions_masks, val_labels)

        val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=args.batchsize, shuffle=False, num_workers=4,
                                                 pin_memory=True)

        return train_dataset, train_loader, val_dataset, val_loader

    else:

        print('Loading validation data...\n')
        val_data = np.load(os.path.join(args.input_data_root, 'test/test_data.npy'),
                           allow_pickle=True)
        print('Loading validation data lesions...\n')
        val_data_lesions_masks = np.load(os.path.join(args.input_data_root, 'test/test_data_lesions.npy'),
                                         allow_pickle=True)
        print('Loading validation labels...\n')
        val_labels = np.load(os.path.join(args.input_data_root, 'test/test_labels.npy'),
                             allow_pickle=True)

        val_dataset = Data(val_data, val_data_lesions_masks, val_labels)

        val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=args.batchsize, shuffle=False, num_workers=4,
                                                 pin_memory=True)

        return val_dataset, val_loader


def train_model(args,model):

    _, train_loader, _, val_loader=load_data(args,model=model)

    ### Checkpoint
    checkpoint = {
        'counters': {
            'best_epoch': None,
        },
        'state': None,
        'optim_state': None,
    }

    train_loss_list = []
    val_loss_list = []
    ### If args.checkpoint_root does not exists create the folder...
    if not os.path.exists(args.checkpoint_output):
        try:
            os.makedirs(args.checkpoint_output)
        except OSError as exception:
            if exception.errno == errno.EEXIST and os.path.isdir(args.checkpoint_output):
                pass
            else:
                raise ValueError(
                    "Failed to created output directory {}...\n".format(args.checkpoint_output))

    ### Optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)

    ###Validation options
    min_val_loss = 100000000000
    min_training_loss = 100000000000

    val_early_stop = 0
    train_early_stop = 0

    ##Start_training...
    start_time = time.time()

    for epoch in range(args.num_epochs):

        epoch_loss=[]

        start_time_epoch = time.time()

        for batch_idx, batch in enumerate(train_loader):

            (slices, lesions, targets) = batch

            max_samples = slices.size()[1]

            x = slices.view(slices.size()[0] * slices.size()[1], slices.size()[2], slices.size()[3], slices.size()[4])
            x = x.to(device=device, dtype=torch.float)

            y = lesions.view(lesions.size()[0] * lesions.size()[1], lesions.size()[2], lesions.size()[3],
                                   lesions.size()[4])
            y = y.to(device=device, dtype=torch.float)

            label = torch.zeros(targets.size()[0] * max_samples, args.number_classes)

            label[range(targets.size()[0] * max_samples), targets.repeat_interleave(max_samples)] = 1

            label = label.to(device=device)

            z_mean, z_log_var, encoded, x_hat = model(x, y, label)

            kl_divergence = torch.mean(0.5 * (z_mean ** 2 +
                                    torch.exp(z_log_var) - z_log_var - 1))

            pixelwise_difference = F.binary_cross_entropy(x_hat, x, reduction='mean')

            batch_loss = pixelwise_difference + args.beta*kl_divergence###the model of the paper beta=1

            epoch_loss.append(batch_loss)

            optimizer.zero_grad()
            batch_loss.backward()
            optimizer.step()

            print('Epoch: %03d/%03d | Batch %03d/%03d | Loss: %.4f...\n'
                  % (epoch + 1, args.num_epochs, (batch_idx+1),
                     len(train_loader), batch_loss))

        loss = torch.mean(torch.stack(epoch_loss))

        print('Epoch: %03d/%03d | Loss: %.4f...\n'
              % (epoch + 1, args.num_epochs, loss))

        if loss < min_training_loss:
            min_training_loss = loss
            train_early_stop += 1
            print('New lower Training Loss at Epoch: %03d/%03d | Minimum Loss: %.4f...\n'
                  % (epoch + 1, args.num_epochs, min_training_loss))

        if abs(epoch - train_early_stop) == args.num_epochs_train_early_stop:
            print('Early Stop at Epoch: %03d/%03d because of now improvements in training after %03d Epochs | Minimum Loss: %.4f...\n'
                  % (epoch + 1, args.num_epochs, args.num_epochs_train_early_stop,min_training_loss))

        ###Validate model
        if (epoch+1) % args.check_every == 0 and epoch>0:

            model.eval()
            val_loss = validate_model(args, epoch, model, val_loader)

            train_loss_list.append(loss.cpu().detach().numpy())

            val_loss_list.append(val_loss.cpu().detach().numpy())

            ###Save the model. This is a little tricky here...
            if val_loss<min_val_loss: #Best model in recostruction does not always offer the best results
                ## See: Higgins, I., Matthey, L., Pal, A., Burgess, C., Glorot, X., Botvinick, M., ... & Lerchner, A. (2016).
                #       beta-vae: Learning basic visual concepts with a constrained variational framework.
                #       "Good reconstructions are associated with entangled representations"

                val_early_stop += 1
                checkpoint['counters']['best_epoch']=epoch
                checkpoint['state'] = model.state_dict()
                checkpoint['optim_state'] = optimizer.state_dict()
                checkpoint_path = os.path.join(
                    args.checkpoint_output, 'model_val_epoch_{}.pt'.format((epoch+1))
                )
                print('Saving checkpoint to {}...\n'.format(checkpoint_path))
                torch.save(checkpoint, checkpoint_path)

            model.train()

            if val_loss < min_val_loss:
                min_val_loss = val_loss
                train_early_stop += 1
                print('New lower Validation Loss at Epoch: %03d/%03d | Minimum Loss: %.4f...\n'
                      % (epoch + 1, args.num_epochs, min_val_loss))

            if abs(epoch % args.check_every - val_early_stop) == args.num_epochs_train_early_stop:
                print(
                    'Early Stop at Epoch: %03d/%03d because of now improvements in validation after %03d Epochs | Minimum Loss: %.4f...\n'
                    % (epoch + 1, args.num_epochs, args.num_epochs_val_early_stop, min_val_loss))


        print('Time elapsed for Epoch: %03d was : %.2f min...\n' % (epoch + 1, (time.time() - start_time_epoch) / 60))

    with open(os.path.join(args.checkpoint_output, 'train_loss.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_loss_list))

    with open(os.path.join(args.checkpoint_output, 'val_loss.npy'), 'w') as outfile:
            np.save(outfile.name, np.asarray(val_loss_list))

    print('Total Training Time: %.2f min...\n' % ((time.time() - start_time) / 60))

def validate_model(args, epoch, model, val_loader):

    val_loss = []

    idx = random.randint(0, (len(val_loader) - 1))

    with torch.no_grad():

        for batch_idx, batch in enumerate(val_loader):

            (slices, lesions, targets) = batch

            max_samples = slices.size()[1]

            x = slices.view(slices.size()[0] * slices.size()[1], slices.size()[2], slices.size()[3],
                                 slices.size()[4])
            x = x.to(device=device, dtype=torch.float)

            y = lesions.view(lesions.size()[0] * lesions.size()[1], lesions.size()[2], lesions.size()[3],
                                   lesions.size()[4])
            y = y.to(device=device, dtype=torch.float)

            label = torch.zeros(targets.size()[0] * max_samples, args.number_classes)

            label[range(targets.size()[0] * max_samples), targets.repeat_interleave(max_samples)] = 1

            label = label.to(device=device)

            ### Forward pass and back propagation
            z_mean, z_log_var, encoded, x_hat = model(x, y, label)

            ### Compute rec_eror

            pixelwise_difference = F.binary_cross_entropy(x_hat, x, reduction='mean')

            batch_loss = pixelwise_difference

            if batch_idx == idx: #choose a random batch

                folder_to_save = args.rec_output.format(
                    (epoch + 1), (batch_idx + 1))

                if not os.path.exists(folder_to_save):
                    try:
                        os.makedirs(folder_to_save)
                    except OSError as exception:
                        if exception.errno == errno.EEXIST and os.path.isdir(folder_to_save):
                            pass
                        else:
                            raise ValueError(
                                "Failed to created output directory '%s'...\n" % folder_to_save)

                slices = x.view(slices.size()[0], slices.size()[1], 1, slices.size()[3], slices.size()[4])

                decoded = x_hat.view(slices.size()[0], slices.size()[1], 1, slices.size()[3], slices.size()[4])

                slices = slices[:, :, :1, :, :]

                idx_in_batch = random.randint(0, (slices.size()[0] - 1))

                for slice in range(max_samples):

                    img = slices[idx_in_batch, slice, :, :, :] * 255
                    img_2 = decoded[idx_in_batch, slice, :, :, :] * 255

                    img = torch.squeeze(img)

                    img_2 = torch.squeeze(img_2)

                    cv2.imwrite(filename=os.path.join(folder_to_save,'{}_Test.png'.format((slice + 1))), img=img.detach().cpu().numpy(),
                                params=(cv2.IMWRITE_PNG_COMPRESSION, cv2.CV_32F))
                    cv2.imwrite(filename=os.path.join(folder_to_save,'{}_Reconst.png'.format((slice + 1))), img=img_2.detach().cpu().numpy(),
                                params=(cv2.IMWRITE_PNG_COMPRESSION, cv2.CV_32F))

            val_loss.append(batch_loss)

        loss = torch.mean(torch.stack(val_loss))

        print('Val_Epoch: %03d | Loss: %.4f...\n' % ((epoch+1), loss))

        return loss

def main(args):
    ### Model
    torch.manual_seed(RANDOM_SEED)

    CVAE = ConditionalVariationalAutoencoder(args.latent_dim, num_classes=args.number_classes,
                                             label_embedding_dim=args.label_embedding_dim,
                                             context_gating_ouput_dim=args.context_gating_ouput_dim)
    CVAE = CVAE.to(device)

    print('Here is the model:')

    print(CVAE)

    if args.is_training:
        CVAE.train()
        train_model(args,CVAE)
    else:
        model = CVAE.eval()
        _, val_loader = load_data(args, model=model)
        epoch = args.epoch
        validate_model(args, epoch=epoch, model=model, val_loader=val_loader)

if __name__ == '__main__':
    argsin = sys.argv[1:]
    args = parser.parse_args(argsin)
    print(args.__dict__)
    main(args)
    sys.exit()

