# -*- coding: utf-8 -*-
##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

            #######################################################################################################
            ###This is an ongoing research project, please do not use this program for autonomous self-diagnosis###
            #######################################################################################################


import torch
import torch.nn as nn
import torch.nn.functional as F
from Training.Loupe_pytorch_train import NetVLAD as NetVLAD
import math

def add_activation_layer(activation):
    if activation == 'relu':
        layer = nn.ReLU(inplace=True)
    elif activation == 'leakyrelu':
        layer = nn.LeakyReLU(inplace=True)
    elif activation == 'elu':
        layer = nn.ELU(inplace=True)
    elif activation == 'tanh':
        layer = nn.Tanh()
    elif activation == 'sigmoid':
        layer = nn.Sigmoid()
    elif activation == 'celu':
        layer = nn.CELU(inplace=True)
    elif activation == 'prelu':
        layer = nn.PReLU()
    return layer

def build_Convd2d(in_channels, out_channels, kernel_size, stride, padding, activation, add_batch_norm=True):
    if add_batch_norm:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            nn.BatchNorm2d(out_channels),
            add_activation_layer(activation=activation)
        )
    else:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            add_activation_layer(activation=activation)
        )
def build_NN(dim_list, activation=None, batch_norm=True, dropout=0):
    layers = []
    for dim_in, dim_out in zip(dim_list[:-1], dim_list[1:]):
        layers.append(nn.Linear(dim_in, dim_out))
        if batch_norm:
            layers.append(nn.BatchNorm1d(dim_out))
        if activation is not None:
            layers.append(add_activation_layer(activation=activation))
        elif activation is None:
            print('non activation')
        if dropout > 0:
            layers.append(nn.Dropout(p=dropout))
    return nn.Sequential(*layers)

class SPP(nn.Module):
    def __init__(self, level_pool_size, pool_operation='max'):
        super(SPP, self).__init__()
        self.level_pool_size = level_pool_size

        if pool_operation=='max':
            self.pool_operation = pool_operation
        else:
            raise ValueError(
                "This pooling operation is not defined {}...\n".format(pool_operation))

    def forward(self, input):
        if len(input.size()) < 4:
            raise ValueError(
                "Expected 4D input size but got { }D...\n".format(len(input.size())))

        if input.size()[-2] < self.level_pool_size[0] or input.size()[-1] < self.level_pool_size[0]:
            raise ValueError(
                "Pyramid pooling size can not be higher than previous filter size{}...\n".format(self.pool_operation))
        bath_size = input.size()[0]
        h, w = input.size()[-2:]

        for i in range(len(self.level_pool_size)):
            h_wid = math.ceil(h / self.level_pool_size[i])
            w_wid = math.ceil(w / self.level_pool_size[i])
            h_str = math.floor(h / self.level_pool_size[i])
            w_str = math.floor(w / self.level_pool_size[i])
            if self.pool_operation == 'max':
                x= F._max_pool2d(input,kernel_size=(h_wid, w_wid), stride=(h_str, w_str))
            if i == 0:
                spp = x.view(bath_size, -1)
            else:
                spp = torch.cat((spp, x.view(bath_size, -1)), 1)
        return spp

class GatingValve(nn.Module):
    def __init__(self, dim, activation='sigmoid', add_batch_norm=True):
        super(GatingValve, self).__init__()
        self.dim = dim
        if add_batch_norm:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=True)
        else:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=False)
        ###############################################
        # Reinitialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()

    def forward(self, x, y):

        attention = torch.mul(x, y)

        attention = attention.view(-1, attention.size()[1] * attention.size()[2] * attention.size()[3])

        gated_content = self.gate(attention)

        return gated_content

##########################
### MODEL
##########################

class CNN_NetVLAD(torch.nn.Module):
    def __init__(self,num_classes,num_clusters=64):
        super(CNN_NetVLAD, self).__init__()

        self.num_classes = num_classes

        self.num_clusters = num_clusters

        self.conv_1 = build_Convd2d(in_channels=1, out_channels=16, kernel_size=(5,5), stride=(2,2), padding=0, activation='leakyrelu', add_batch_norm=False)

        self.pool_1 = nn.MaxPool2d(kernel_size=(2,2),stride=(2,2),padding=0)

        self.conv_2 = build_Convd2d(in_channels=16, out_channels=32, kernel_size=(3, 3), stride=(2, 2), padding=0, activation='leakyrelu', add_batch_norm=False)

        self.pool_2 = nn.MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0)

        self.conv_3 = build_Convd2d(in_channels=32, out_channels=64, kernel_size=(3, 3), stride=(2, 2), padding=0, activation='leakyrelu', add_batch_norm=False)

        self.conv_1_valve = build_Convd2d(in_channels=1, out_channels=16, kernel_size=(5, 5), stride=(2, 2), padding=0,
                                    activation='leakyrelu', add_batch_norm=False)

        self.conv_2_valve = build_Convd2d(in_channels=16, out_channels=32, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                    activation='leakyrelu', add_batch_norm=False)

        self.conv_3_valve = build_Convd2d(in_channels=32, out_channels=64, kernel_size=(3, 3), stride=(2, 2), padding=0,
                                    activation='leakyrelu', add_batch_norm=False)

        self.spp = SPP(level_pool_size=[5,3,2],pool_operation='max')

        self.fc_1 = build_NN([3136, 512], activation='leakyrelu', batch_norm=False, dropout=0.0)

        self.VLAD = NetVLAD(feature_size=512, cluster_size=self.num_clusters, output_dim=512, gating=True, add_batch_norm=False)

        self.fc_2 = nn.Linear(512,self.num_classes)

        ###############################################
        # Reinitialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()
            elif isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()
    def forward(self, input_data, input_lesions,max_samples):

        x = self.conv_1(input_data)
        x= self.pool_1(x)

        x = self.conv_2(x)
        x = self.pool_2(x)

        x = self.conv_3(x)

        y = self.conv_1_valve(input_lesions)
        y= self.pool_1(y)

        y = self.conv_2_valve(y)
        y = self.pool_2(y)

        y = self.conv_3_valve(y)

        ###############
        # Equation (2) # Valve filter with side information
        ##############
        gated = torch.mul(x,y)

        x = F.relu(gated, inplace=True)

        x = self.spp(x)

        x = self.fc_1(x)

        x = self.VLAD(x,max_samples)

        logits = self.fc_2(x)

        proba = F.softmax(logits, dim=1)

        return logits, proba