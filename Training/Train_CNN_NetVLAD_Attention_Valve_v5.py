# -*- coding: utf-8 -*-
##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

#######################################################################################################
###This is an ongoing research project, please do not use this program for autonomous self-diagnosis###
#######################################################################################################

import argparse
import errno
import os
import time

import matplotlib
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from Training.CNN_NetVLAD_Attention_Valve_v5 import CNN_NetVLAD
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_curve, auc
from torch.utils.data import Dataset
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import sys

##########################
### SETTINGS
##########################
parser = argparse.ArgumentParser()
RANDOM_SEED = 0
##########################
### Data
##########################
parser.add_argument('--input_data', metavar='input_data', type=str,
                    default='./',
                    help='Path to the root folder with the training data')
parser.add_argument('--checkpoint_root', metavar='percent_of_samples_to_val', type=float,
                    help='The portion of samples to use as training set. A number between 0 and 1',
                    default=0.5)
parser.add_argument('--is_training', metavar='is_training', type=bool,
                    default=True,
                    help='Define if the model is in training or evaluations mode')
##########################
### Model Optimization
##########################
parser.add_argument('--num_epochs', metavar='epochs', type=int,
                    default=500,
                    help='Define if the number of epochs to train')

parser.add_argument('--learning_rate', metavar='learning_rate', type=float, help='Define the learning rate',
                    default=1e-5)

parser.add_argument('--weight_decay', metavar='weight_decay', default=1e-5, type=float)

parser.add_argument('--pos_weight', metavar='pos_weight', default=0.35, type=float)

parser.add_argument('--neg_weight', metavar='neg_weight', default=0.25, type=float)

parser.add_argument('--gamma', metavar='gamma', default=2.0, type=float)

parser.add_argument('--clip_threshold', metavar='clip_threshold', default=5.0, type=float)

parser.add_argument('--checkpoint_output', metavar='checkpoint_output', type=str,
                    default='./',
                    help='Path to the root folder to save the checkpoint')

parser.add_argument('--batchsize', type=int, default=5,
                    help="Number of volumes to process simultaneously. Lower number requires less memory but may be slower")

parser.add_argument('--check_every', type=int, default=5,
                    help="amount of epoch to perform validation")

parser.add_argument('--num_epochs_train_early_stop', metavar='num_epochs_train_early_stop', type=int,
                    default=50, help='Number of epochs to stop if non improvements regarding the train loss')

parser.add_argument('--num_epochs_val_early_stop', metavar='num_epochs_val_early_stop', type=int,
                    default=50, help='Number of epochs to stop if non improvements regarding the validation loss')

##########################
### Pre-trained Classification model, mainly for iterative training###
### Please, do note that you must probably need to play with the hyper-parameters and take into account the data
### distribution to improve the validation metrics with an iterative training.
##########################

parser.add_argument('--pretrained_model', metavar='pretrained_model', type=bool,
                    default=True,
                    help='Optimize pretrained model')

parser.add_argument('--checkpoint_load', metavar='checkpoint_load', type=str,
                    default='./',
                    help='Path to the root folder with the previous checkpoint saved')

parser.add_argument('--checkpoint_epoch', metavar='checkpoint_epoch', type=int,
                    default=0,
                    help='epoch_to load')

##########################
### Pre-trained Autoencoder
##########################
parser.add_argument('--pretrained_AE', metavar='pretrained_AE', type=bool, default=False,
                    help='Initialize weights from CVAE')

parser.add_argument('--checkpoint_output_AE', metavar='checkpoint_output', type=str,
                    default='./',
                    help='Path to the root folder to save the checkpoint')

parser.add_argument('--epoch', metavar='epoch', type=int,
                    default=0,
                    help='best_checkpoint')

parser.add_argument('--freeze_CVAE', metavar='freeze_hyper_parameters', type=bool, default=False,
                    help='Keep frozen the hyper-parameters from the CVAE encoder')
##########################
### Model Parameters
##########################
parser.add_argument('--number_classes', type=int,
                    help="Number of classes",
                    default=2)
parser.add_argument('--number_clusters', type=int,
                    help="Number of clusters",
                    default=64)
####TODO correctly
# if torch.cuda.is_available():
#     torch.backends.cudnn.deterministic = True
# Device
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Device:', device)


##########################
# Data Loader
##########################
class Data(Dataset):
    def __init__(self, data_set, data_lesions, labels):
        super(Data, self).__init__()
        self.data_set = data_set
        self.data_lesions = data_lesions
        self.labels = labels

    def __len__(self):
        return len(self.data_set)

    def __getitem__(self, idx):
        return [self.data_set[idx, :, :, :].astype(float), self.data_lesions[idx, :, :, :].astype(float),
                self.labels[idx].astype(int)]


##########################
### Load train data
##########################
def load_data(args):
    ###input_folder=args.input_data
    print('Loading training data...\n')
    train_data = np.load(
        os.path.join(args.input_data,
                     'train/train_data.npy'), allow_pickle=True)
    print('Loading training lesions...\n')
    train_data_lesions = np.load(
        os.path.join(args.input_data,
                     'train/train_data_lesions.npy'), allow_pickle=True)
    print('Loading training labels...\n')
    train_labels = np.load(
        os.path.join(args.input_data,
                     'train/train_labels.npy'), allow_pickle=True)

    train_dataset = Data(train_data, train_data_lesions, train_labels)

    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batchsize, shuffle=True, num_workers=4,
                                               pin_memory=True)

    print('Loading validation data lesions...\n')
    val_data = np.load(os.path.join(args.input_data, 'val/val_data.npy'),
                       allow_pickle=True)
    print('Loading validation data lesions...\n')
    val_data_lesions = np.load(os.path.join(args.input_data, 'val/val_data_lesions.npy'),
                               allow_pickle=True)
    print('Loading validation labels...\n')
    val_labels = np.load(os.path.join(args.input_data, 'val/val_labels.npy'),
                         allow_pickle=True)

    val_dataset = Data(val_data, val_data_lesions, val_labels)

    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=args.batchsize, shuffle=True, num_workers=4,
                                             pin_memory=True)

    return train_dataset, train_loader, val_dataset, val_loader


class FocalLoss(nn.Module):

    def __init__(self, weight=None,
                 gamma=2., reduction='none'):
        nn.Module.__init__(self)
        self.weight = weight
        self.gamma = gamma
        self.reduction = reduction

    def forward(self, input_tensor, target_tensor):
        log_prob = F.log_softmax(input_tensor, dim=-1)
        prob = torch.exp(log_prob)
        return F.nll_loss(
            ((1 - prob) ** self.gamma) * log_prob,
            target_tensor,
            weight=self.weight,
            reduction=self.reduction
        )


def compute_roc_auc(args, epoch, visualize, labels, prediction):
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(args.number_classes):
        fpr[i], tpr[i], _ = roc_curve(labels == i, prediction == i)
        roc_auc[i] = auc(fpr[i], tpr[i])
        if visualize:
            plt.figure()
            lw = 1
            plt.plot(fpr[i], tpr[i], color='darkorange',
                     lw=lw, label='ROC curve (area = %0.3f)' % roc_auc[i])
            plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')
            if i == 0:
                plt.title('ROC Class: NON-COVID')
            elif i == 1:
                plt.title('ROC Class: COVID')
            plt.legend(loc="lower right")
            plt.savefig(os.path.join(args.checkpoint_output, 'ROC_Epoch{}_class{}.jpeg'.format(epoch, i)))
            plt.close('all')

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(labels.ravel(), prediction.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    if visualize:
        plt.figure()
        lw = 1
        plt.plot(fpr["micro"], tpr["micro"], color='darkorange',
                 lw=lw, label='ROC curve (area = %0.2f)' % roc_auc["micro"])

        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC')
        plt.legend(loc="lower right")
        plt.savefig(os.path.join(args.checkpoint_output, 'ROC_Epoch{}_AVE.jpeg'.format(epoch)))
        plt.close('all')

    return roc_auc["micro"]


def train_model(args):
    _, train_loader, _, val_loader = load_data(args)

    ### Checkpoint
    checkpoint = {
        'counters': {
            'best_epoch': None,
        },
        'state': None,
        'optim_state': None,
    }

    train_sensitivity_list = []
    val_sensitivity_list = []

    train_specificity_list = []
    val_specificity_list = []

    train_loss_list = []
    val_loss_list = []

    train_accuracy_list = []
    val_accuracy_list = []

    ### If args.checkpoint_root does not exists create the folder...
    if not os.path.exists(args.checkpoint_output):
        try:
            os.makedirs(args.checkpoint_output)
        except OSError as exception:
            if exception.errno == errno.EEXIST and os.path.isdir(args.checkpoint_output):
                pass
            else:
                raise ValueError(
                    "Failed to created output directory {}...\n".format(args.checkpoint_output))

    ### Model
    # torch.manual_seed(RANDOM_SEED)
    model = CNN_NetVLAD(args.number_classes, args.number_clusters)

    model = model.to(device)

    print('Here is the model:')

    print(model)

    if args.pretrained_model:
        checkpoint_path_model = os.path.join(
            args.checkpoint_load, 'model_val_epoch_{}.pt'.format(args.checkpoint_epoch)
        )

        checkpoint_load = torch.load(checkpoint_path_model, map_location=device)

        model = model.to(device)

        model.load_state_dict(checkpoint_load['state'])

    if args.pretrained_AE:
        ### Checkpoint
        checkpoint_path_AE = os.path.join(
            args.checkpoint_output_AE, 'model_val_epoch_{}.pt'.format(args.epoch)
        )

        checkpoint_AE = torch.load(checkpoint_path_AE, map_location=device)

        model.conv_1._modules['0'].weight.data = checkpoint_AE['state']['conv_1.0.weight']

        model.conv_1._modules['0'].bias.data = checkpoint_AE['state']['conv_1.0.bias']

        model.conv_2._modules['0'].weight.data = checkpoint_AE['state']['conv_2.0.weight']

        model.conv_2._modules['0'].bias.data = checkpoint_AE['state']['conv_2.0.bias']

        model.conv_3._modules['0'].weight.data = checkpoint_AE['state']['conv_3.0.weight']

        model.conv_3._modules['0'].bias.data = checkpoint_AE['state']['conv_3.0.bias']

        model.conv_1_valve._modules['0'].weight.data = checkpoint_AE['state']['conv_1_valve.0.weight']

        model.conv_1_valve._modules['0'].bias.data = checkpoint_AE['state']['conv_1_valve.0.bias']

        model.conv_2_valve._modules['0'].weight.data = checkpoint_AE['state']['conv_2_valve.0.weight']

        model.conv_2_valve._modules['0'].bias.data = checkpoint_AE['state']['conv_2_valve.0.bias']

        model.conv_3_valve._modules['0'].weight.data = checkpoint_AE['state']['conv_3_valve.0.weight']

        model.conv_3_valve._modules['0'].bias.data = checkpoint_AE['state']['conv_3_valve.0.bias']

    if args.freeze_CVAE:

        for p in model.conv_1.parameters():
            p.requires_grad = False

        for p in model.conv_2.parameters():
            p.requires_grad = False

        for p in model.conv_3.parameters():
            p.requires_grad = False

        for p in model.conv_1_valve.parameters():
            p.requires_grad = False

        for p in model.conv_2_valve.parameters():
            p.requires_grad = False

        for p in model.conv_3_valve.parameters():
            p.requires_grad = False

    ### Optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay)

    pos_weight = torch.tensor([args.neg_weight, args.pos_weight])

    pos_weight = pos_weight.to(device=device)

    criterion = FocalLoss(weight=pos_weight, gamma=args.gamma, reduction='sum')

    start_time = time.time()

    ###Validation options
    max_val_sensitivity = 0
    max_training_sensitivity = 0

    max_val_specificity = 0
    max_training_specificty = 0

    max_val_acc = 0
    max_val_ave_auc = 0

    # max_training_acc = 0
    max_training_ave_auc = 0

    val_early_stop = 0
    train_early_stop = 0

    ##Start_training...
    for epoch in range(args.num_epochs):

        epoch_loss = 0
        num_examples = 0
        correct_pred = 0

        start_time_epoch = time.time()

        for batch_idx, batch in enumerate(train_loader):

            (slices, lesions, targets) = batch

            # batchsize = slices.size()[0]

            max_samples = slices.size()[1]

            slices = slices.view(slices.size()[0] * slices.size()[1], slices.size()[2], slices.size()[3],
                                 slices.size()[4])

            lesions = lesions.view(lesions.size()[0] * lesions.size()[1], lesions.size()[2], lesions.size()[3],
                                   lesions.size()[4])

            lesions = lesions.to(device=device, dtype=torch.float)
            slices = slices.to(device=device, dtype=torch.float)
            targets = targets.to(device=device, dtype=torch.long)

            logits, probas = model(slices, lesions, max_samples)

            ### Compute loss

            y = targets

            batch_loss = criterion(logits, y)

            epoch_loss += batch_loss

            ### Uptate parameters
            optimizer.zero_grad()

            batch_loss.backward()

            if args.clip_threshold > 0.0:
                torch.nn.utils.clip_grad_norm_(
                    model.parameters(), max_norm=args.clip_threshold
                )

            optimizer.step()
            # scheduler.step()

            _, predicted_labels = torch.max(probas, 1)

            num_examples += targets.size()[0]
            correct_pred += (predicted_labels == targets).sum()

            if batch_idx == 0:
                prediction = predicted_labels.detach().cpu().numpy()
                labels = targets.detach().cpu().numpy()
            else:
                prediction = np.concatenate((prediction, predicted_labels.detach().cpu().numpy()))
                labels = np.concatenate((labels, targets.detach().cpu().numpy()))

        print('Training metrics report at epoch: %03d/%03d: ' % ((epoch + 1), args.num_epochs))

        print(classification_report(y_true=labels, y_pred=prediction,
                                    target_names=['NON-COVID', 'COVID']))

        precision, recall, fscore, support = precision_recall_fscore_support(y_true=labels,
                                                                             y_pred=prediction,
                                                                             average=None)
        train_convid_sensitivity = recall[1]

        train_convid_specificity = recall[0]

        train_acc = correct_pred.float() / num_examples * 100

        train_loss = epoch_loss.float() / len(train_loader)

        train_ave_auc = compute_roc_auc(args, epoch + 1, visualize=False, labels=labels, prediction=prediction)

        print(
            'Training metrics: %03d/%03d | Loss: %.5f | Accuraccy: %.4f | Sensitivity: %.4f | Specificity: %.4f | AUC_AVE: %.4f......\n'
            % (epoch + 1, args.num_epochs, train_loss, train_acc, train_convid_sensitivity, train_convid_specificity,
               train_ave_auc))
        train_early_stop += 1
        if train_convid_sensitivity > max_training_sensitivity or train_convid_specificity > max_training_specificty or train_ave_auc > max_training_ave_auc:
            train_early_stop = 0
            if train_convid_sensitivity > max_training_sensitivity:
                max_training_sensitivity = train_convid_sensitivity
                print('New Higher Training Sensitivity at Epoch: %03d/%03d | Maximun Sensitivity: %.4f...\n'
                      % (epoch + 1, args.num_epochs, train_convid_sensitivity))
            if train_convid_specificity > max_training_specificty:
                max_training_specificty = train_convid_specificity
                print('New Higher Training Specificity at Epoch: %03d/%03d | Maximun Specificity %.4f...\n'
                      % (epoch + 1, args.num_epochs, train_convid_specificity))

        if train_early_stop == args.num_epochs_train_early_stop:
            print(
                'Early Stop at Epoch: %03d/%03d because of now improvements in training after %03d Epochs | Minimum Sensitivity: %.4f...\n'
                % (epoch + 1, args.num_epochs, args.num_epochs_train_early_stop, max_training_sensitivity))
            break
        ###Validate model
        if (epoch + 1) % args.check_every == 0 and epoch > 0:
            ###Put model in evaluation mode
            val_ave_auc, val_loss, val_acc, val_convid_sensitivity, val_convid_specificity = validate_model(args, epoch,
                                                                                                            model,
                                                                                                            val_loader)

            train_sensitivity_list.append(train_convid_sensitivity)
            val_sensitivity_list.append(val_convid_sensitivity)

            train_specificity_list.append(train_convid_specificity)
            val_specificity_list.append(val_convid_specificity)

            train_loss_list.append(train_loss.detach().cpu().numpy())
            val_loss_list.append(val_loss.detach().cpu().numpy())

            train_accuracy_list.append(train_acc.detach().cpu().numpy())
            val_accuracy_list.append(val_acc.detach().cpu().numpy())

            ###Save the model
            val_early_stop += 1

            if val_ave_auc >= max_val_ave_auc or val_convid_sensitivity >= max_val_sensitivity or val_acc >= max_val_acc or val_convid_specificity >= max_val_specificity:

                val_early_stop = 0

                if val_ave_auc > max_val_ave_auc:
                    max_val_ave_auc = val_ave_auc
                    print('New Higher Validation AVE_AUC at Epoch: %03d/%03d | '
                          'Maximun AVE_AUC: %.4f...\n'
                          % (epoch + 1, args.num_epochs, max_val_ave_auc))
                if val_convid_sensitivity > max_val_sensitivity:
                    max_val_sensitivity = val_convid_sensitivity
                    print('New Higher Validation Sensitivity at Epoch: %03d/%03d | '
                          'Maximun Sensitivity: %.4f...\n'
                          % (epoch + 1, args.num_epochs, max_val_sensitivity))
                if val_acc > max_val_acc:
                    max_val_acc = val_acc
                    print('New Higher Validation Accuracy at Epoch: %03d/%03d | '
                          'Maximun Acccuracy: %.4f...\n'
                          % (epoch + 1, args.num_epochs, max_val_acc))
                if val_convid_specificity > max_val_specificity:
                    max_val_specificity = val_convid_specificity
                    print('New Higher Validation Specificity at Epoch: %03d/%03d | '
                          'Maximun Specificity: %.4f...\n'
                          % (epoch + 1, args.num_epochs, max_val_specificity))

                print(
                    'Ultimate validation metrics at Epoch: %03d/%03d | Loss: %.5f | Acccuracy: %.4f | Sensitivity: %.4f | Specificity: %.4f | AUC_AVE: %.4f...\n'
                    % (epoch + 1, args.num_epochs, val_loss, val_acc, val_convid_sensitivity, val_convid_specificity,
                       val_ave_auc))
                checkpoint['counters']['best_epoch'] = epoch
                checkpoint['state'] = model.state_dict()
                checkpoint['optim_state'] = optimizer.state_dict()
                checkpoint_path = os.path.join(
                    args.checkpoint_output, 'model_val_epoch_{}.pt'.format((epoch + 1))
                )
                print('Saving checkpoint to {}...\n'.format(checkpoint_path))
                torch.save(checkpoint, checkpoint_path)

            ###Put model in evaluation mode
            # model.train()

            if val_early_stop == args.num_epochs_train_early_stop:
                print(
                    'Early Stop at Epoch: %03d/%03d because of now improvements in validation after %03d Epochs | Minimum Sensitivity: %.4f...\n'
                    % (epoch + 1, args.num_epochs, args.num_epochs_train_early_stop, max_val_sensitivity))
                break

        print('Time elapsed for Epoch: %03d was : %.2f min...\n' % (epoch + 1, (time.time() - start_time_epoch) / 60))

    with open(os.path.join(args.checkpoint_output, 'train_sensitivity.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_sensitivity_list))

    with open(os.path.join(args.checkpoint_output, 'val_sensitivity.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(val_sensitivity_list))

    with open(os.path.join(args.checkpoint_output, 'train_specificity.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_specificity_list))

    with open(os.path.join(args.checkpoint_output, 'val_specificity.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(val_specificity_list))

    with open(os.path.join(args.checkpoint_output, 'train_acc.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_accuracy_list))

    with open(os.path.join(args.checkpoint_output, 'val_acc.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(val_accuracy_list))

    with open(os.path.join(args.checkpoint_output, 'train_loss.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(train_loss_list))

    with open(os.path.join(args.checkpoint_output, 'val_loss.npy'), 'w') as outfile:
        np.save(outfile.name, np.asarray(val_loss_list))

    print('Total Training Time: %.2f min...\n' % ((time.time() - start_time) / 60))


def main(args):
    train_model(args)


def validate_model(args, epoch, model, val_loader):
    val_loss = 0

    correct_pred = 0

    num_examples = 0

    pos_weight = torch.tensor([args.neg_weight, args.pos_weight])

    pos_weight = pos_weight.to(device=device)

    criterion = FocalLoss(weight=pos_weight, gamma=args.gamma, reduction='sum')

    with torch.no_grad():

        model.eval()

        for batch_idx, batch in enumerate(val_loader):

            (slices, lesions, targets) = batch

            # batchsize = slices.size()[0]

            max_samples = slices.size()[1]

            slices = slices.view(slices.size()[0] * slices.size()[1], slices.size()[2], slices.size()[3],
                                 slices.size()[4])

            lesions = lesions.view(lesions.size()[0] * lesions.size()[1], lesions.size()[2], lesions.size()[3],
                                   lesions.size()[4])

            lesions = lesions.to(device=device, dtype=torch.float)

            slices = slices.to(device=device, dtype=torch.float)

            targets = targets.to(device=device, dtype=torch.long)

            ### Forward pass
            logits, probas = model(slices, lesions, max_samples)

            ## Compute loss

            y = targets

            val_loss += criterion(logits, y)

            _, predicted_labels = torch.max(probas, 1)
            num_examples += targets.size()[0]
            correct_pred += (predicted_labels == targets).sum()

            if batch_idx == 0:
                prediction = predicted_labels.detach().cpu().numpy()
                labels = targets.detach().cpu().numpy()
            else:
                prediction = np.concatenate((prediction, predicted_labels.detach().cpu().numpy()))
                labels = np.concatenate((labels, targets.detach().cpu().numpy()))

        print('Validation metrics report at epoch: %03d/%03d: ' % ((epoch + 1), args.num_epochs))

        print(classification_report(y_true=labels, y_pred=prediction,
                                    target_names=['NON-COVID', 'COVID']))

        precision, recall, fscore, support = precision_recall_fscore_support(y_true=labels,
                                                                             y_pred=prediction,
                                                                             average=None)
        convid_sensitivity = recall[1]

        convid_specificity = recall[0]

        acc = correct_pred.float() / num_examples * 100

        loss = val_loss.float() / len(val_loader)

        AVE_AUC = compute_roc_auc(args, epoch + 1, visualize=False, labels=labels, prediction=prediction)

        print(
            'Validation metrics at Epoch: %03d/%03d | Loss: %.5f | Acccuracy: %.4f | Sensitivity: %.4f | Specificity: %.4f | AVE_AUC: %.4f...\n'
            % (epoch + 1, args.num_epochs, loss, acc, convid_sensitivity, convid_specificity, AVE_AUC))

    model.train()

    return AVE_AUC, loss, acc, convid_sensitivity, convid_specificity


if __name__ == '__main__':
    argsin = sys.argv[1:]
    args = parser.parse_args(argsin)
    print(args.__dict__)
    main(args)
    sys.exit()
