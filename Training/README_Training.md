## Description:

This folder contains the code for training the CT-Based COVID-19 Diagnostic Tool.

## Files to use

- Train_CVAE_NET_v1.py
- Train_CNN_NetVLAD_Attention_Valve_v5.py

Both files are created assuming data have been preprocessed as .npy files. See utils.py for the preprocessing steps

## Inputs

Three numpy (.npy) files

1.  The masked lung CT-scan ({phase}_data.npy)
2.  The binary lesion segmentation masks resulting from lesion segmentation({phase}_data_lesions.npy)
3.  The labels ({phase}_labels.npy)

Note: Data is assumed to be located in a path like: args.input_data/{phase}/{phase}_data.npy. Phases: train, val, test

## Instructions:

1. Change the parameters concerning the paths.

2. Train the CVAE (Train_CVAE_NET_v1.py)

2. Train the Classification model (Train_CNN_NetVLAD_Attention_Valve_v5.py)
