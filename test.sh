#!/bin/sh

python3 Test_CNN_NetVLAD_Attention_Valve_v5.py \
	--input_data_root ./Processed_data \
	--checkpoint_output ./Model \
	--batchsize 1 \
	--number_of_slices 80 \
							
