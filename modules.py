import torch.nn as nn
import math
import torch
import torch.nn.functional as F
from torch import nn as nn


class SPP(nn.Module):
    def __init__(self, level_pool_size, pool_operation='max'):
        super(SPP, self).__init__()
        self.level_pool_size = level_pool_size

        if pool_operation=='max':
            self.pool_operation = pool_operation
        else:
            raise ValueError(
                "This pooling operation is not defined {}...\n".format(pool_operation))

        self.pool_output_sizes = []

    def forward(self, input):
        if len(input.size()) < 4:
            raise ValueError(
                "Expected 4D input size but got { }D...\n".format(len(input.size())))

        if input.size()[-2] < self.level_pool_size[0] or input.size()[-1] < self.level_pool_size[0]:
            raise ValueError(
                "Pyramid pooling size can not be higher than previous filter size{}...\n".format(self.pool_operation))


        bath_size = input.size()[0]
        h, w = input.size()[-2:]
        self.pool_output_sizes = []

        for i in range(len(self.level_pool_size)):

            h_wid = math.ceil(h / self.level_pool_size[i])
            w_wid = math.ceil(w / self.level_pool_size[i])
            h_str = math.floor(h / self.level_pool_size[i])
            w_str = math.floor(w / self.level_pool_size[i])
            # print('(', h, w, ')')
            # print('window:', h_wid, w_wid)
            # print('stride:', h_str, w_str)
            if self.pool_operation == 'max':
                x= F._max_pool2d(input,kernel_size=(h_wid, w_wid), stride=(h_str, w_str))
            # elif self.pool_operation == 'mean':
            #     x = nn.AdaptiveAvgPool2d(kernel_size=(h_wid, w_wid), stride=(h_str, w_str))
            if i == 0:
                spp = x.view(bath_size, -1)
            else:
                spp = torch.cat((spp, x.view(bath_size, -1)), 1)

            self.pool_output_sizes.append(x.shape)

        return spp


class GatingValve(nn.Module):
    def __init__(self, dim, activation='sigmoid', add_batch_norm=True):
        super(GatingValve, self).__init__()
        self.dim = dim
        if add_batch_norm:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=True)
        else:
            self.gate = build_NN([dim, dim], activation=activation, batch_norm=False)
        ###############################################
        # Reinitialize weights using He initialization
        ###############################################
        for m in self.modules():
            if isinstance(m, torch.nn.Linear):
                nn.init.kaiming_normal_(m.weight.detach())
                m.bias.detach().zero_()

    def forward(self, x, y):

        attention = torch.mul(x, y)

        attention = attention.view(-1, attention.size()[1] * attention.size()[2] * attention.size()[3])

        gated_content = self.gate(attention)

        #gated_content = gated_content * x.view(-1, x.size()[1] * x.size()[2] * x.size()[3])

        return gated_content


def add_activation_layer(activation):

    # LRP: Only supports ReLU and LeakyReLU here

    if activation == 'relu':
        layer = nn.ReLU(inplace=True)
    elif activation == 'leakyrelu':
        layer = nn.LeakyReLU(inplace=True)
    else:
        raise ValueError('{} is not a valid activation function for LRP'.format(activation))
    '''elif activation == 'elu':
        layer = nn.ELU(inplace=True)
    elif activation == 'tanh':
        layer = nn.Tanh()
    elif activation == 'sigmoid':
        layer = nn.Sigmoid()
    elif activation == 'celu':
        layer = nn.CELU(inplace=True)
    elif activation == 'prelu':
        layer = nn.PReLU()'''
    return layer


def build_Convd2d(in_channels, out_channels, kernel_size, stride, padding, activation, add_batch_norm=True):
    if add_batch_norm:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            nn.BatchNorm2d(out_channels),
            add_activation_layer(activation=activation)
        )
    else:
        return torch.nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            add_activation_layer(activation=activation)
        )


def build_NN(dim_list, activation=None, batch_norm=True, dropout=0):
    layers = []
    for dim_in, dim_out in zip(dim_list[:-1], dim_list[1:]):
        layers.append(nn.Linear(dim_in, dim_out))
        if batch_norm:
            layers.append(nn.BatchNorm1d(dim_out))
        if activation is not None:
            layers.append(add_activation_layer(activation=activation))
        elif activation is None:
            print('non activation')
        if dropout > 0:
            layers.append(nn.Dropout(p=dropout))
    return nn.Sequential(*layers)