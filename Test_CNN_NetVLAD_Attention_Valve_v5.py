# -*- coding: utf-8 -*-
##########################################################################################
# (c) Copyright 2020
# The author(s):    Abel Díaz Berenguer
#                   Boris Joukovsky (LRP compatibility)
#                   Maryna Kvasnytsia
#                   Ine Dirks 
#
# Vrije Universiteit Brussel,
# Department of Electronics and Informatics (ETRO),
# Faculty of Engineering Sciences,
# VUB-ETRO,
# 2 Pleinlaan, 1050 Belgium.
# (VUB - ETRO)
#
# Interuniversity Microelectronics Centre (IMEC),
# Kapeldreef 75, 3001 Heverlee, Belgium
# (IMEC)
#
# All right reserved
# Permission to use, copy, modify, and distribute within VUB-ETRO & IMEC
# this routine/program/software and/or its documentation, or part of any one of them
# (the program) for research purpose within VUB-ETRO & IMEC is hereby granted, provided:
# (1) that the copyright notice appears in all copies,
# (2) that both the copyright notice and this permission notice appear in the supporting documentation, and
# (3) that all publications based partially or completely on this program will have the main publications of "the authors" related to this
# work cited as references.
# Any other type of handling or use of the program, CAN NOT BE DONE
# WITHOUT PRIOR WRITTEN APPROVAL from the VUB-ETRO & IMEC,
# including but not restricted to the following:
# (A) any re-distribution of the program to any person outside of the VUB-ETRO & IMEC
# (B) any use of the program which is not academic research;
# (C) any commercial activity involving the use of the program is strictly prohibited.
# Modifications: 2018
# Disclaimer: VUB-ETRO & IMEC make no claim about the
# suitability of the program for any particular purpose.
# It is provided "as is" without any form of warranty or support.
##########################################################################################

            #######################################################################################################
            ###This is an ongoing research project, please do not use this program for autonomous self-diagnosis###
            #######################################################################################################

import argparse
import errno
import os
import SimpleITK as sitk
import numpy as np
import torch
from torch.utils.data import Dataset
from CNN_NetVLAD_Attention_Valve_v5 import CNN_NetVLAD
from visual import visuals
import sys

##########################
### SETTINGS
##########################
parser = argparse.ArgumentParser()
RANDOM_SEED = 0
###TODO Correct the input data parameters
parser.add_argument('--input_data_root', metavar='input_data_root', type=str, default='./Processed_data',
                    help='Path to the root folder with the 3 CT-scans original CT, lung mask segmentation and lesions segmentation')

parser.add_argument('--checkpoint_output', metavar='checkpoint_output', type=str,
                    default='./Model',
                    help='Path to the root folder with the model')

parser.add_argument('--batchsize', type=int, default=1,
                    help="Number of volumes (1 volume per patient).")
###################
# Model Parameters#
###################
parser.add_argument('--number_of_slices', type=int, default=80,
                    help="Number of slices per volumes to process")

###############
# Explaination#
###############
parser.add_argument('--explain', default=False, action='store_true',
                    help='Computes the explaination heatmap')

parser.add_argument('--target', type=int, default=1,
                    help="The label for a certain patient 0: Negative and 1: Positive. This is required for the exlaination method")

####TODO Correct
# if torch.cuda.is_available():
#     torch.backends.cudnn.deterministic = True
# Device
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Device:', device)

##############
# Data Loader#
##############
class Data_Evaluation(Dataset):

    def __init__(self, data_set, data_lesions,labels):
        super(Data_Evaluation, self).__init__()
        self.data_set = data_set
        self.data_lesions = data_lesions
        self.labels = labels

    def __len__(self):
        return len(self.data_set)

    def __getitem__(self, idx):
        return [self.data_set[idx, :, :, :].astype(float), self.data_lesions[idx, :, :, :].astype(float),
                self.labels[idx].astype(int)]
#########################
# Data Loader Inference #
#########################
class Data_Inferece(Dataset):

    def __init__(self, data_set, data_lesions):
        super(Data_Inferece, self).__init__()
        self.data_set = data_set
        self.data_lesions = data_lesions

    def __len__(self):
        return len(self.data_set)

    def __getitem__(self, idx):
        return [self.data_set[idx, :, :, :].astype(float), self.data_lesions[idx, :, :, :].astype(float)]

#######################################
# Auxiliary functions to load the data#
#######################################

def binarize_image(image):
    print('Binarizing image...')
    thresholder = sitk.BinaryThresholdImageFilter()
    thresholder.SetLowerThreshold(0.5)
    thresholder.SetUpperThreshold(1)
    thresholder.SetOutsideValue(0)
    thresholder.SetInsideValue(1)
    binary_image = thresholder.Execute(image)
    return binary_image

def normalize(image_array):
    print('Normalizing image...')
    min_bound = -1000.0
    max_bound = 400.0
    image_array = (image_array - min_bound) / (max_bound - min_bound)
    image_array[image_array > 1] = 1.
    image_array[image_array < 0] = 0.
    return image_array

def load_data(args): #### This is not well-optimized classes for data access should be properly structured/inheritance

    print('Loading evaluation data, this may take a few minutes...\n')

    path_to_original_ct_scanfile = os.path.join(args.input_data_root,'ct.nii.gz')

    path_to_lung_mask = os.path.join(args.input_data_root,'ct_mask.nii.gz')

    path_to_lesions_segmentation = os.path.join(args.input_data_root,'ct_lesions.nii.gz')

    assert (os.path.exists(path_to_original_ct_scanfile) is True), 'This file {} .... does not exist...\n'.format(path_to_original_ct_scanfile)

    assert (os.path.exists(path_to_original_ct_scanfile) is True), 'This file {} .... does not exist...\n'.format(
        path_to_lung_mask)

    assert (os.path.exists(path_to_original_ct_scanfile) is True), 'This file {} .... does not exist...\n'.format(
        path_to_lesions_segmentation)

    image_ct = sitk.ReadImage(path_to_original_ct_scanfile)  # get ct_image#

    image_lung_mask = sitk.ReadImage(path_to_lung_mask)  #get lungs mask#

    image_lesion_mask = sitk.ReadImage(path_to_lesions_segmentation)  # get ct_image#

    if image_ct.GetSize() != image_lung_mask.GetSize() != image_lesion_mask.GetSize():
        print('Equal sizes are required....')

        print('Size image is: {}'.format(image_ct.GetSize()))

        print('Size mask is: {}'.format(image_lesion_mask.GetSize()))

        print('Size lesion is: {}'.format(image_lesion_mask.GetSize()))

        sys.exit()

    if image_ct.GetSize() != image_lung_mask.GetSize() != image_lesion_mask.GetSize():
        print('Equal sizes are required....')

        print('Size image is: {}'.format(image_ct.GetSize()))

        print('Size mask is: {}'.format(image_lesion_mask.GetSize()))

        print('Size lesion is: {}'.format(image_lesion_mask.GetSize()))

        sys.exit()

    if image_ct.GetDimension() < 3 or image_lung_mask.GetDimension() < 3 or image_lesion_mask.GetDimension() < 3:
        print('Equal sizes are required....')

        print('DIM image is: {}'.format(image_ct.GetDimension()))

        print('DIM mask is: {}'.format(image_lesion_mask.GetDimension()))

        print('DIM lesion is: {}'.format(image_lesion_mask.GetDimension()))

        sys.exit()

    elif image_ct.GetWidth() != image_ct.GetHeight() or (image_ct.GetWidth() < 512): #### Could be lower than
                                                                                     #### 512  but we assumed these dimensions
        print('Square Slices of with dim equal are required....')

        print('Width is: {}'.format(image_ct.GetWidth()))

        print('Height is: {}'.format(image_ct.GetHeight()))

        sys.exit()

    ### Get the metadata from files to be used in explainability
    origin = image_ct.GetOrigin()
    spacing = image_ct.GetSpacing()
    direction = image_ct.GetDirection()
    subrate = image_ct.GetSize()[2] / args.number_of_slices
    spacing = (spacing[0], spacing[1], spacing[2] * subrate)
    metadata = (origin, direction, spacing)


    ct_array = sitk.GetArrayFromImage(image_ct)[:, 20:492, 20:492]

    lung_mask_array = sitk.GetArrayFromImage(binarize_image(image_lung_mask))[:, 20:492, 20:492]

    lesions_array = sitk.GetArrayFromImage(binarize_image(image_lesion_mask))[:, 20:492, 20:492]
    idx_zero = np.where(lesions_array == 0)
    idx_one = np.where(lesions_array == 1)

    lesions_array[idx_zero] = 1
    lesions_array[idx_one] = 0

    norm_ct_array = normalize(ct_array)

    masked_ct_array = norm_ct_array * lung_mask_array

    max_value_z = args.number_of_slices

    if ct_array.shape[0] > max_value_z:

        idx = np.arange(start=np.random.randint(0, (ct_array.shape[0] / max_value_z)),
                        step=(ct_array.shape[0] / max_value_z),
                        stop=ct_array.shape[0]).astype(int)

        if idx.shape[0] > max_value_z:

           idx = idx[:max_value_z]

        eval_data = masked_ct_array[idx, :, :]

        eval_data_lesions = lesions_array[idx, :, :]

    else:

        eval_data = masked_ct_array[:, :, :]

        eval_data_lesions = lesions_array[:, :, :]

    eval_data = np.expand_dims(eval_data, axis=0)
    eval_data = np.expand_dims(eval_data, axis=2)

    eval_data_lesions = np.expand_dims(eval_data_lesions, axis=0)
    eval_data_lesions = np.expand_dims(eval_data_lesions, axis=2)

    eval_dataset = Data_Inferece(eval_data, eval_data_lesions)

    eval_loader = torch.utils.data.DataLoader(eval_dataset, batch_size= 1, shuffle=False, num_workers=4,
                                             pin_memory=True)

    return eval_dataset, eval_loader, metadata

#################################
# Function to evaluate the model#
#################################
def evaluate_model(args):

    ### Model

    torch.manual_seed(RANDOM_SEED)

    model = CNN_NetVLAD(2)

    ### Checkpoint
    checkpoint_path = os.path.join(
        args.checkpoint_output, 'model.pt'
    )

    checkpoint = torch.load(checkpoint_path, map_location=device)

    model = model.to(device)

    model.load_state_dict(checkpoint['state'])

    model.eval()

    print('Here is the model:')

    print(model)

    _, eval_loader,_ = load_data(args)

    with torch.no_grad():

        for batch_idx, batch in enumerate(eval_loader):

            (slices, lesions) = batch

            max_samples = slices.size()[1]

            slices = slices.view(slices.size()[0] * slices.size()[1], slices.size()[2], slices.size()[3], slices.size()[4])

            lesions = lesions.view(lesions.size()[0] * lesions.size()[1], lesions.size()[2], lesions.size()[3],
                          lesions.size()[4])

            lesions = lesions.to(device=device, dtype=torch.float)

            slices = slices.to(device=device, dtype=torch.float)

            ### Forward pass
            _, probas = model(slices, lesions, max_samples)

            _, predicted_labels = torch.max(probas, 1)  ### 0 -> NonCovid   1 -> COVID
                                                        ### Threshold is assumed to be equal to 0.5
        if predicted_labels:
            print('It seems that patient is Positive to COVID-19')
        else:
            print('It seems that patient is Negative to COVID-19')
    sys.exit()


def explain_model(args):

    ### Model
    torch.manual_seed(RANDOM_SEED)
    model = CNN_NetVLAD(2)

    ### Checkpoint
    checkpoint_path = os.path.join(
        args.checkpoint_output, 'model.pt'
    )

    checkpoint = torch.load(checkpoint_path, map_location=device)

    model = model.to(device)
    model.load_state_dict(checkpoint['state'])

    # LRP: Converting model using LRP aware modules
    model.convert_to_lrp_model()

    model.eval()

    print('Here is the model:')
    print(model)

    _, eval_loader, metadata = load_data(args)

    targets = np.zeros_like([0])

    targets[0] = args.target####Correct

    for batch_idx, batch in enumerate(eval_loader):

        model.zero_grad()
        torch.cuda.empty_cache()

        key = batch_idx

        (slices, lesions) = batch

        max_samples = slices.size()[1]
        slices = slices.view(slices.size()[0] * slices.size()[1], slices.size()[2], slices.size()[3], slices.size()[4])
        lesions = lesions.view(lesions.size()[0] * lesions.size()[1], lesions.size()[2], lesions.size()[3],
                               lesions.size()[4])
        lesions = lesions.to(device=device, dtype=torch.float)
        slices = slices.to(device=device, dtype=torch.float)

        ### Forward pass
        logits , probas = model(slices, lesions, max_samples, lrp_aware=args.explain)

        y = targets

        _, predicted_labels = torch.max(probas, 1)

        if predicted_labels[0] == 0 and targets[0] == 0:
            det = 'TN'
        elif predicted_labels[0] == 1 and targets[0] == 0:
            det = 'FP'
        elif predicted_labels[0] == 0 and targets[0] == 1:
            det = 'FN'
        else:
            det = 'TP'

        if det != 'TP':
            print('Error: explanations only for Positive cases.')
        else:
            print('Generating the explanations for a Positive case')
            # Explain using LRP: Positive class
            Ry = torch.zeros_like(logits, dtype=predicted_labels.dtype)
            for idx in range(args.batchsize):
                Ry[idx, 0] = 0.0
                Ry[idx, 1] = 1.0 if logits[idx,1] >= 0 else -1.0

            R_lung, R_lesions = model.lrp(Ry, y, lrp_feat='alphabeta', lrp_pool='simple', lrp_classifier='simple')
            # Simple guided back-propagation

            R_lung_covid = R_lung.detach().cpu().numpy()
            R_lesions_covid = R_lesions.detach().cpu().numpy()

            normfac = np.sqrt(np.mean(R_lung_covid[R_lung_covid > 0] ** 2)) * 2

            visuals.output_sitk(slices, R_lung_covid, os.path.join(args.input_data_root+'/output', str(key) + '_' + det), 'C_lungs', normfac, metadata)
            visuals.output_sitk(lesions, R_lesions_covid, os.path.join(args.input_data_root+'/output', str(key) + '_' + det), 'C_lesion', normfac, metadata)
            
            del R_lung, R_lesions, Ry, batch, slices, lesions
    sys.exit()

def main(args):
    assert (args.batchsize == 1), 'Error: please set the batch_size to 1'
    if args.explain is True:
        assert (args.target == 1), 'Error: explanations only for Positive cases.'
        explain_model(args)
    else:
        with torch.no_grad():
            evaluate_model(args)

if __name__ == '__main__':
    argsin = sys.argv[1:]
    args = parser.parse_args(argsin)
    main(args)